package com.addcel.api.dataaccess.components.encryption;

public interface Encryptionable {

	public String execute(String data);
}
