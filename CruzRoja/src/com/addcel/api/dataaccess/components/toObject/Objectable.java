package com.addcel.api.dataaccess.components.toObject;

import com.addcel.api.addcelexception.OwnException;

public interface Objectable {

	public void execute(String data) throws OwnException;
	public Object getData();
}
