package com.addcel.api.dataaccess.components.implementation.connect;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.connection.Connectable;
import com.addcel.prosa.Start;

/*
	Some other notes on GET requests:
	
	GET requests can be cached
	GET requests remain in the browser history
	GET requests can be bookmarked
	GET requests should never be used when dealing with sensitive data
	GET requests have length restrictions
	GET requests should be used only to retrieve data
*/

public class MethodGET implements Connectable {

	private HttpConnection connection;
	private String url = null;
	private String json = null;
	
	public MethodGET(String url){
		this.url = url;
	}
	
	public String createURL() {
		
		if (json == null){
			return url + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
		} else {
			return url + "?json=" + json + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
		}
	}
	
	public void setUrl(String url){
		this.url = url;
	}
	
	
	public void execute(String json) throws OwnException {

		this.json = json;
		byte datas[] = null;
		ByteArrayOutputStream responseBytes = null;
		DataInputStream in = null;
		
		try {
			
			connection = (HttpConnection) Connector.open(createURL(), Connector.READ_WRITE, true);

			connection.setRequestMethod(HttpConnection.GET);
			connection.setRequestProperty("User-Agent", "Profile/MIDP-1.0 Confirguration/CLDC-1.0");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			in = connection.openDataInputStream();

			int length = (int) connection.getLength();

			if (length != -1) {
				datas = new byte[length];
				in.readFully(datas);
			} else {
				responseBytes = new ByteArrayOutputStream();
				int ch;
				while ((ch = in.read()) != -1) {
					responseBytes.write(ch);
				}
				datas = responseBytes.toByteArray();
			}
			
			this.json = new String(datas);
			
		} catch (IOException e) {
			throw new OwnException(Error.HTTP_SEND_DATA);
		} finally {

			try {
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public Object getData() {
		return json;
	}
}

