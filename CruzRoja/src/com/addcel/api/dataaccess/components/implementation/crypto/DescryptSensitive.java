package com.addcel.api.dataaccess.components.implementation.crypto;

import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.util.security.AddcelCrypto;

public class DescryptSensitive implements Descryptionable{

	private String data;
	
	public String execute(String data) {

		return AddcelCrypto.decryptSensitive(data);
	}


}
