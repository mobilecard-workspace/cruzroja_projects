package com.addcel.api.dataaccess.components.implementation.crypto;

import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.util.security.AddcelCrypto;

// Encryptor

public class EncryptHard implements Encryptionable {

	String data = null;
	
	public String execute(String data) {

		return AddcelCrypto.encryptHard(data);
	}
}
