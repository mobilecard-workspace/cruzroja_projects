package com.addcel.api.dataaccess.components.implementation.crypto;

import java.util.Date;

import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.util.security.AddcelCrypto;

public class EncryptSensitive implements Encryptionable{

	private String data;
	
	public String execute(String data) {

		Date hoy = new Date();
		long lkey = hoy.getTime();
		String skey = String.valueOf(lkey);
		
		int length = skey.length();
		
		skey = skey.substring(length - 8, length);
		
		return AddcelCrypto.encryptSensitive(skey, data);
	}
}
