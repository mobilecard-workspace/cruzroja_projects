package com.addcel.api.dataaccess;

import java.io.IOException;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.implementation.connect.MethodGET;
import com.addcel.prosa.view.base.Viewable;


public class DAVersionamiento extends DataAccessible implements Runnable {

	private Viewable customMainScreen;
	private String url;
	private String json;
	private int request;

	public DAVersionamiento(Viewable customMainScreen, String url, String json, int request){
		
		super(customMainScreen, json);
		
		this.customMainScreen = customMainScreen;
		this.url = url;
		this.json = json;
		this.request = request;
	}
	
	
	public void run() {
		
		execute();
	}
	
	
	protected void execute() {
		
		try {

			MethodGET methodGET = new MethodGET(url);
			 methodGET.execute(null);
			String jsonHTTP = (String) methodGET.getData();
			
			JSONObject jsonObject = new JSONObject(jsonHTTP);
			
			customMainScreen.setData(request, jsonObject);
			
		} catch (OwnException e) {
			e.printStackTrace();
			customMainScreen.setData(DataAccessible.NUM_ERROR_IO, DataAccessible.DESCRIPTION_ERROR_IO);
		} catch (JSONException e) {
			e.printStackTrace();
			customMainScreen.setData(DataAccessible.NUM_ERROR_JSON, DataAccessible.DESCRIPTION_ERROR_JSON);
		}
	}


	protected void execute(int command, String URL, String json,
			Viewable mainScreen) {
	}


	protected void execute(Viewable viewable, String json) {
		// TODO Auto-generated method stub
		
	}
}