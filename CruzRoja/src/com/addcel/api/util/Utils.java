package com.addcel.api.util;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Vector;


import net.rim.device.api.crypto.SHA1Digest;
import net.rim.device.api.math.Fixed32;
import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.EncodedImage;
import net.rim.device.api.system.GPRSInfo;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.util.StringUtilities;

import org.w3c.dom.Node;

public class Utils {
	
	public static boolean validDate(String _month, String _year) {
		Calendar c = Calendar.getInstance();
		Calendar c1 = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 0);
		c.set(Calendar.MONTH, Integer.parseInt(_month));
		c.set(Calendar.YEAR, 2013 + Integer.parseInt(_year));
		if (c1.after(c)) {
			return false;
		} else {
			return true;
		}
			
		// String month = c.get(Calendar.MONTH);
		// String year = c.get(Calendar.YEAR);
	}


	public static ServiceRecord getWAP2ServiceRecord() {
		ServiceBook sb = ServiceBook.getSB();
		ServiceRecord[] records = sb.getRecords();

		for (int i = 0; i < records.length; i++) {
			String cid = records[i].getCid().toLowerCase();
			String uid = records[i].getUid().toLowerCase();
			if (cid.indexOf("wptcp") != -1 && uid.indexOf("wifi") == -1
					&& uid.indexOf("mms") == -1) {
				return records[i];
			}
		}

		return null;
	}
/*
	static byte[] getImagefromURL(String url) {
		// long time = System.currentTimeMillis();

		int progress = 0;
		InputStream in = null;
		byte[] data = new byte[256];
		try {
			HttpConnection conn = (HttpConnection) Connector.open(url + ";"
					+ MainClass.idealConnection);
			in = conn.openInputStream();
			// conn.close();
			DataBuffer db = new DataBuffer();
			int chunk = 0;
			while (-1 != (chunk = in.read(data))) {
				progress += chunk;
				db.write(data, 0, chunk);
			}
			in.close();
			data = db.getArray();
			// MainClass.screen.add(new
			// RichTextField("imagen: "+(System.currentTimeMillis()-time)));
		} catch (Exception e) {
			// if(conn!=null)
			// {
			// try {
			// conn.close();
			// } catch (IOException e1) {
			// e1.printStackTrace();
			// }
			// }
		}
		return data;
	}
*/
	static Node findTag(String s, Node node) {
		if (node.getNodeName().equals(s)) {
			return node;
			// fm.add(new RichTextField ("un item"));

		} else {
			for (int i = 0; i < node.getChildNodes().getLength(); i++) {
				if (node.getChildNodes().item(i).equals(s)) {
					return node.getChildNodes().item(i);
					// fm.add(new RichTextField ("un item"));
				} else {
					findTag(s, node.getChildNodes().item(i));
				}
			}
		}
		return null;
	}

	static public String getServiceBookApn() {
		ServiceBook sb = ServiceBook.getSB();
		ServiceRecord[] records = sb
				.findRecordsByType(ServiceRecord.SRT_ACTIVE);
		String apn = null;

		for (int i = 0; i < records.length; i++) {
			ServiceRecord sr = records[i];

			if (StringUtilities.strEqualIgnoreCase(sr.getCid(), "WPTCP")
					&& StringUtilities.strEqualIgnoreCase(sr.getUid(),
							"WAP2 trans")) {
				apn = sr.getUid();
			}
		}

		return apn;
	}

	public static String[] splitString(String cadena, String patron) {
		Vector items = new Vector();
		int posicion = 0;
		posicion = cadena.indexOf(patron);
		while (posicion >= 0) {
			items.addElement(cadena.substring(0, posicion).trim());
			cadena = cadena.substring(posicion + patron.length());
			posicion = cadena.indexOf(patron);
		}
		items.addElement(cadena);

		// pasamos vector a un String[]
		String[] itemsStr = new String[items.size()];
		for (int a = 0; a < items.size(); a++) {
			itemsStr[a] = items.elementAt(a).toString();
		}

		return itemsStr;
	}

	static public String getDateText(String fecha) {
		String date = null;
		Calendar c = Calendar.getInstance();
		int pos = fecha.indexOf("/");
		// Dialog.alert(" "+fecha.substring(pos+1, fecha.indexOf("-", pos+1)));
		c.set(Calendar.DATE, Integer.parseInt(fecha.substring(0, pos)));
		// c.set(Calendar.DATE, Integer.parseInt(fecha.substring(0,2)));
		c.set(Calendar.MONTH,
				Integer.parseInt(fecha.substring(pos + 1,
						fecha.indexOf("/", pos + 1))) - 1);
		//
		// c.set(Calendar.YEAR, 2010);
		// c.set(Calendar.HOUR_OF_DAY,
		// Integer.parseInt(fecha.substring(11,13)));
		// c.set(Calendar.MINUTE, Integer.parseInt(fecha.substring(14,16)));

		if (c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
			// date = "Lunes, "+Integer.parseInt(fecha.substring(0,2));
			// date =
			// "Lunes, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Lun";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
			// date =
			// "Martes, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Mar";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
			// date =
			// "Mi�rcoles, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Mie";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
			// date =
			// "Jueves, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Jue";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			// date =
			// "Viernes, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Vie";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			// date =
			// "S�bado, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Sab";
		} else if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			// date =
			// "Domingo, "+Integer.parseInt(fecha.substring(0,fecha.indexOf("-")));
			date = "Dom";
		}

		// if(c.get(Calendar.MONTH) == Calendar.JANUARY)
		// {
		// date = date + " de Enero de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.FEBRUARY)
		// {
		// date = date + " de Febrero de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.MARCH)
		// {
		// date = date + " de Marzo de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.APRIL)
		// {
		// date = date + " de Abril de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.MAY)
		// {
		// date = date + " de Mayo de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.JUNE)
		// {
		// date = date + " de Junio de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.JULY)
		// {
		// date = date + " de Julio de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.AUGUST)
		// {
		// date = date + " de Agosto de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.SEPTEMBER)
		// {
		// date = date + " de Septiembre de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.OCTOBER)
		// {
		// date = date + " de Octubre de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.NOVEMBER)
		// {
		// date = date + " de Noviembre de 2010";
		// }
		// else if(c.get(Calendar.MONTH) == Calendar.DECEMBER)
		// {
		// date = date + " de Diciembre de 2010";
		// }

		return date;
	}

	static public int pixelToChar(int length, Font g) {
		int tamano;
		int tamanoCaracter = g.getAdvance("a");
		tamano = length / tamanoCaracter;
		return tamano;
	}

	// static public int paintText(Graphics g, String temp, int x, int y, int
	// limit) {
	// int pos =0, i=0, b = 0;
	// String cad = "";
	// if (temp.length()>limit)
	// {
	// while(temp.length()>limit)
	// {
	// pos = temp.indexOf(" ", limit);
	// if(pos>0)
	// {
	// pos++;
	// if(b == 1){
	// // cad = temp.substring(0,pos-1)+"...";
	// cad = temp.substring(0,pos-1);
	// }else{
	// cad = temp.substring(0,pos);
	// }
	// g.drawText(cad, x , y+(i*g.getFont().getHeight()));
	// temp = temp.substring(pos);
	// b++;
	// if(b == 2){
	// break;
	// }
	// }
	// else
	// {
	// g.drawText(temp, x, y+(i*g.getFont().getHeight()));
	// break;
	// }
	//
	// i++;
	// }
	// if(b != 2){
	// g.drawText(temp, x, y+(i*g.getFont().getHeight()));
	// }
	//
	// return y+((i+1)*g.getFont().getHeight());
	// }
	// else
	// {
	// g.drawText(temp, x, y);
	// return y+g.getFont().getHeight();
	// }
	//
	// }

	static public int paintText(Graphics g, String temp, int x, int y, int limit) {
		int pos = 0, i = 0;

		if (temp.length() > limit) {
			while (temp.length() > limit) {
				pos = temp.indexOf(" ", limit);
				if (pos > 0) {
					pos++;
					g.drawText(temp.substring(0, pos), x, y
							+ (i * g.getFont().getHeight()));
					temp = temp.substring(pos);

				} else {
					g.drawText(temp, x, y + (i * g.getFont().getHeight()));
					break;
				}
				i++;
			}
			g.drawText(temp, x, y + (i * g.getFont().getHeight()));
			return y + ((i + 1) * g.getFont().getHeight());
		} else {
			g.drawText(temp, x, y);
			return y + g.getFont().getHeight();
		}

	}

	static public int paintTextOriginal(Graphics g, String temp, int x, int y,
			int limit) {
		int pos = 0, i = 0;

		if (temp.length() > limit) {
			while (temp.length() > limit) {
				pos = temp.indexOf(" ", limit);
				if (pos > 0) {
					pos++;
					g.drawText(temp.substring(0, pos), x, y
							+ (i * g.getFont().getHeight()));
					temp = temp.substring(pos);

				} else {
					g.drawText(temp, x, y + (i * g.getFont().getHeight()));
					break;
				}
				i++;
			}
			g.drawText(temp, x, y + (i * g.getFont().getHeight()));
			return y + ((i + 1) * g.getFont().getHeight());
		} else {
			g.drawText(temp, x, y);
			return y + g.getFont().getHeight();
		}

	}

	static public int paintTextMiddle(Graphics g, String temp, int x, int y,
			int limit) {
		int pos = 0, i = 0;

		if (temp.length() > limit) {
			while (temp.length() > limit) {
				pos = temp.indexOf(" ", limit);
				if (pos > 0) {
					pos++;
					g.drawText(temp.substring(0, pos), (Display.getWidth() - g
							.getFont().getAdvance(temp.substring(0, pos))) / 2,
							y + (i * g.getFont().getHeight()));
					temp = temp.substring(pos);

				} else {
					g.drawText(temp, (Display.getWidth() - g.getFont()
							.getAdvance(temp)) / 2, y
							+ (i * g.getFont().getHeight()));
					break;
				}
				i++;
			}
			g.drawText(temp,
					(Display.getWidth() - g.getFont().getAdvance(temp)) / 2, y
							+ (i * g.getFont().getHeight()));
			return y + ((i + 1) * g.getFont().getHeight());
		} else {
			g.drawText(temp,
					(Display.getWidth() - g.getFont().getAdvance(temp)) / 2, y);
			return y + g.getFont().getHeight();
		}

	}

	static public int cuentaRenglones(Font g, String temp, int x, int y,
			int limit) {
		int pos = 0, i = 0;

		if (temp.length() > limit) {
			while (temp.length() > limit) {
				pos = temp.indexOf(" ", limit);
				if (pos > 0) {
					pos++;
					// g.drawText(temp.substring(0,pos), x ,
					// y+(i*g.getFont().getHeight()));
					temp = temp.substring(pos);

				} else {
					// g.drawText(temp, x, y+(i*g.getFont().getHeight()));
					break;
				}
				i++;
			}
			// g.drawText(temp, x, y+(i*g.getFont().getHeight()));
			return y + ((i + 1) * g.getHeight()) + 1;
		} else {
			// g.drawText(temp, x, y);
			return y + g.getHeight() + 1;
		}

	}

	public static String cleanHTMLFromTags(String s) {
		// MainClass.screen.add(new RichTextField("before"+s));
		String test = null;
		char[] newString = null;

		if (s.length() > 0) {
			newString = new char[s.length()];
			int posString = 0;
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '>')
					i++;
				if (s.charAt(i) == '<') {
					i++;
					switch (s.charAt(i)) {
					case 'p':
						i = (s.indexOf('>', i + 1));
						break;
					case 'P':
						i = (s.indexOf('>', i + 1));
						break;
					case '/':
						if (s.charAt(i + 2) == 'p') {
							Dialog.alert("p");
						} else {
							i = (s.indexOf('>', i + 1));
						}
						break;
					case 's':
						i = (s.indexOf('>', i + 1));
						break;
					case 'S':
						i = (s.indexOf('>', i + 1));
						break;
					case 'b':
						i = (s.indexOf('>', i + 1));
						break;
					case 'B':
						i = (s.indexOf('>', i + 1));
						break;
					}
					// if(i<s.length())
					// newString[posString++] = s.charAt(i);
				}
				// else
				{
					if (i < s.length())
						newString[posString++] = s.charAt(i);
				}

			}
			if (newString != null)
				test = new String(newString);

			test = test.replace('>', ' ');
			test = test.trim();
			for (int j = 0; j < test.length(); j++) {
				if (test.charAt(j) == ' ' && test.charAt(j + 1) == ' ') {
					test = test.substring(0, j).concat(test.substring(j + 2));
					j--;
				}
			}
			test = test.trim();
			return test;
		} else {
			return " ";
		}
	}

	public static int textWidth(String s, Font font) {
		int temp = 0;

		for (int i = 0; i < s.length(); i++) {
			// if(s.charAt(i) == ' ')
			// {
			// temp = temp + (font.getHeight()/2);
			// }
			// else
			{

				if (font.getHeight() <= 15) {
					if (s.charAt(i) == ' ') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) - 3;
						} else {
							temp = temp + (font.getHeight() / 2) - 6;
						}
					}
					if (s.charAt(i) == 'i') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) - 2;
						} else {
							temp = temp + (font.getHeight() / 2);
						}
					}
					if (s.charAt(i) == 'l' || s.charAt(i) == 'j'
							|| s.charAt(i) == '�' || s.charAt(i) == '('
							|| s.charAt(i) == ')' || s.charAt(i) == '.'
							|| s.charAt(i) == ',' || s.charAt(i) == '\''
							|| s.charAt(i) == ':') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) - 1;
						} else {
							temp = temp + (font.getHeight() / 2) - 3;
						}
					}
					if (s.charAt(i) == '1') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2);
						} else {
							temp = temp + (font.getHeight() / 2) - 2;
						}
					}
					if (s.charAt(i) == 't' || s.charAt(i) == '-'
							|| s.charAt(i) == 'I') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2);
						} else {
							temp = temp + (font.getHeight() / 2) - 2;
						}
					}
					if (s.charAt(i) == 'J') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 1;
						} else {
							temp = temp + (font.getHeight() / 2) - 1;
						}
					}
					if (s.charAt(i) == 'f' || s.charAt(i) == 'r'
							|| s.charAt(i) == '"' || s.charAt(i) == '_') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 1;
						} else {
							temp = temp + (font.getHeight() / 2) - 1;
						}
					}
					if (s.charAt(i) == 'a' || s.charAt(i) == '�'
							|| s.charAt(i) == 'd' || s.charAt(i) == '4'
							|| s.charAt(i) == 'y' || s.charAt(i) == 'b'
							|| s.charAt(i) == 'c' || s.charAt(i) == 'e'
							|| s.charAt(i) == '�' || s.charAt(i) == 'g'
							|| s.charAt(i) == 'h' || s.charAt(i) == 'n'
							|| s.charAt(i) == '�' || s.charAt(i) == 'x'
							|| s.charAt(i) == 'z' || s.charAt(i) == 's'
							|| s.charAt(i) == 'v' || s.charAt(i) == '?'
							|| s.charAt(i) == '�') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 2;
						} else {
							temp = temp + (font.getHeight() / 2);
						}
					}

					if (s.charAt(i) == 'G' || s.charAt(i) == 'E'
							|| s.charAt(i) == 'F' || s.charAt(i) == '2'
							|| s.charAt(i) == '3' || s.charAt(i) == '5'
							|| s.charAt(i) == '6' || s.charAt(i) == '7'
							|| s.charAt(i) == '8' || s.charAt(i) == '9'
							|| s.charAt(i) == '0' || s.charAt(i) == 'u'
							|| s.charAt(i) == '�' || s.charAt(i) == 'k'
							|| s.charAt(i) == 'o' || s.charAt(i) == '�'
							|| s.charAt(i) == 'p' || s.charAt(i) == 'q'
							|| s.charAt(i) == 'L' || s.charAt(i) == 'S') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 3;
						} else {
							temp = temp + (font.getHeight() / 2) - 1;
						}
					}

					if (s.charAt(i) == 'P' || s.charAt(i) == 'T'
							|| s.charAt(i) == 'C' || s.charAt(i) == '#'
							|| s.charAt(i) == 'B' || s.charAt(i) == 'D'
							|| s.charAt(i) == 'H' || s.charAt(i) == 'R'
							|| s.charAt(i) == 'U' || s.charAt(i) == 'V'
							|| s.charAt(i) == 'X' || s.charAt(i) == 'Y'
							|| s.charAt(i) == 'Z') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 4;
						} else {
							temp = temp + (font.getHeight() / 2) + 2;
						}
					}

					if (s.charAt(i) == 'M' || s.charAt(i) == 'A'
							|| s.charAt(i) == 'w' || s.charAt(i) == 'K'
							|| s.charAt(i) == 'N' || s.charAt(i) == '�'
							|| s.charAt(i) == 'O' || s.charAt(i) == 'Q') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 5;
						} else {
							temp = temp + (font.getHeight() / 2) + 2;
						}
					}
					if (s.charAt(i) == 'm') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 6;
						} else {
							temp = temp + (font.getHeight() / 2) + 4;
						}
					}

					if (s.charAt(i) == 'W') {
						if (font.getHeight() >= 14) {
							temp = temp + (font.getHeight() / 2) + 8;
						} else {
							temp = temp + (font.getHeight() / 2) + 6;
						}
					}

				}
				if (font.getHeight() >= 16 && font.getHeight() <= 25) {
					if (s.charAt(i) == ' ')
						temp = temp + (font.getHeight() / 2) - 4;
					if (s.charAt(i) == 'i' || s.charAt(i) == '.'
							|| s.charAt(i) == ',' || s.charAt(i) == '\''
							|| s.charAt(i) == ':' || s.charAt(i) == 'I')
						temp = temp + (font.getHeight() / 2) - 3;
					if (s.charAt(i) == 'j' || s.charAt(i) == 'l'
							|| s.charAt(i) == '�' || s.charAt(i) == '('
							|| s.charAt(i) == ')' || s.charAt(i) == 'J')
						temp = temp + (font.getHeight() / 2) - 2;
					if (s.charAt(i) == 't' || s.charAt(i) == '"')
						temp = temp + (font.getHeight() / 2) - 1;
					if (s.charAt(i) == 'f' || s.charAt(i) == 'r'
							|| s.charAt(i) == '1' || s.charAt(i) == '-')
						temp = temp + (font.getHeight() / 2);
					if (s.charAt(i) == 'a' || s.charAt(i) == '�'
							|| s.charAt(i) == 'c' || s.charAt(i) == 's'
							|| s.charAt(i) == '�' || s.charAt(i) == '_')
						temp = temp + (font.getHeight() / 2) + 1;
					if (s.charAt(i) == 'b' || s.charAt(i) == 'd'
							|| s.charAt(i) == 'e' || s.charAt(i) == 'g'
							|| s.charAt(i) == 'h' || s.charAt(i) == 'n'
							|| s.charAt(i) == 'o' || s.charAt(i) == '�'
							|| s.charAt(i) == 'u' || s.charAt(i) == 'v'
							|| s.charAt(i) == 'x' || s.charAt(i) == 'z'
							|| s.charAt(i) == '�' || s.charAt(i) == '�'
							|| s.charAt(i) == '�' || s.charAt(i) == '2'
							|| s.charAt(i) == '7' || s.charAt(i) == '?')
						temp = temp + (font.getHeight() / 2) + 2;
					if (s.charAt(i) == 'k' || s.charAt(i) == 'p'
							|| s.charAt(i) == 'q' || s.charAt(i) == 'y'
							|| s.charAt(i) == '3' || s.charAt(i) == '4'
							|| s.charAt(i) == '5' || s.charAt(i) == '6'
							|| s.charAt(i) == '8' || s.charAt(i) == '9'
							|| s.charAt(i) == '0' || s.charAt(i) == 'E'
							|| s.charAt(i) == 'F' || s.charAt(i) == 'L'
							|| s.charAt(i) == 'S' || s.charAt(i) == '#')
						temp = temp + (font.getHeight() / 2) + 3;
					if (s.charAt(i) == 'B' || s.charAt(i) == 'C'
							|| s.charAt(i) == 'G' || s.charAt(i) == 'P'
							|| s.charAt(i) == 'R' || s.charAt(i) == 'T'
							|| s.charAt(i) == 'U' || s.charAt(i) == 'V'
							|| s.charAt(i) == 'X' || s.charAt(i) == 'Y'
							|| s.charAt(i) == 'Z')
						temp = temp + (font.getHeight() / 2) + 4;
					if (s.charAt(i) == 'A' || s.charAt(i) == 'D'
							|| s.charAt(i) == 'H' || s.charAt(i) == 'K'
							|| s.charAt(i) == 'N' || s.charAt(i) == '�'
							|| s.charAt(i) == 'O' || s.charAt(i) == 'Q')
						temp = temp + (font.getHeight() / 2) + 5;
					if (s.charAt(i) == 'w' || s.charAt(i) == 'M')
						temp = temp + (font.getHeight() / 2) + 7;
					if (s.charAt(i) == 'm' || s.charAt(i) == 'W')
						temp = temp + (font.getHeight() / 2) + 9;
				}
				if (font.getHeight() >= 26) {
					if (s.charAt(i) == ' ') {
						if (font.getHeight() <= 40)
							temp = temp + (font.getHeight() / 2) - 10;
						else
							temp = temp + (font.getHeight() / 2) - 13;
					}
					if (s.charAt(i) == '\'')
						temp = temp + (font.getHeight() / 2) - 10;
					// if()
					// temp = temp +(font.getHeight()/2)-9;
					if (s.charAt(i) == 'i' || s.charAt(i) == 'l'
							|| s.charAt(i) == '.' || s.charAt(i) == ','
							|| s.charAt(i) == ':')
						temp = temp + (font.getHeight() / 2) - 8;
					if (s.charAt(i) == '�' || s.charAt(i) == '('
							|| s.charAt(i) == ')' || s.charAt(i) == 'J')
						temp = temp + (font.getHeight() / 2) - 6;
					if (s.charAt(i) == '-' || s.charAt(i) == 'I'
							|| s.charAt(i) == '#')
						temp = temp + (font.getHeight() / 2) - 5;
					if (s.charAt(i) == 'j' || s.charAt(i) == 't'
							|| s.charAt(i) == '"')
						temp = temp + (font.getHeight() / 2) - 4;
					if (s.charAt(i) == 'f' || s.charAt(i) == 'r'
							|| s.charAt(i) == '1' || s.charAt(i) == '_')
						temp = temp + (font.getHeight() / 2) - 2;
					// if(s.charAt(i) == 'I')
					// temp = temp +(font.getHeight()/2)-1;
					if (s.charAt(i) == 'c')
						temp = temp + (font.getHeight() / 2) + 1;
					if (s.charAt(i) == 'a' || s.charAt(i) == '�'
							|| s.charAt(i) == 'z' || s.charAt(i) == '?'
							|| s.charAt(i) == '�')
						temp = temp + (font.getHeight() / 2) + 2;
					if (s.charAt(i) == 's')
						temp = temp + (font.getHeight() / 2) + 3;
					if (s.charAt(i) == 'b' || s.charAt(i) == 'd'
							|| s.charAt(i) == 'e' || s.charAt(i) == 'g'
							|| s.charAt(i) == 'h' || s.charAt(i) == 'n'
							|| s.charAt(i) == 'o' || s.charAt(i) == '�'
							|| s.charAt(i) == 'u' || s.charAt(i) == 'v'
							|| s.charAt(i) == 'x' || s.charAt(i) == 'y'
							|| s.charAt(i) == '�' || s.charAt(i) == '�'
							|| s.charAt(i) == '�' || s.charAt(i) == '2'
							|| s.charAt(i) == '3' || s.charAt(i) == '4'
							|| s.charAt(i) == '7' || s.charAt(i) == 'F')
						temp = temp + (font.getHeight() / 2) + 4;
					if (s.charAt(i) == 'q' || s.charAt(i) == '5'
							|| s.charAt(i) == '6' || s.charAt(i) == '8'
							|| s.charAt(i) == '9' || s.charAt(i) == 'E'
							|| s.charAt(i) == 'L')
						temp = temp + (font.getHeight() / 2) + 5;
					if (s.charAt(i) == 'k' || s.charAt(i) == 'p'
							|| s.charAt(i) == '0')
						temp = temp + (font.getHeight() / 2) + 6;
					if (s.charAt(i) == 'B' || s.charAt(i) == 'C'
							|| s.charAt(i) == 'G' || s.charAt(i) == 'S')
						temp = temp + (font.getHeight() / 2) + 7;
					if (s.charAt(i) == 'A' || s.charAt(i) == 'P'
							|| s.charAt(i) == 'R' || s.charAt(i) == 'T'
							|| s.charAt(i) == 'U' || s.charAt(i) == 'V'
							|| s.charAt(i) == 'X' || s.charAt(i) == 'Y'
							|| s.charAt(i) == 'Z')
						temp = temp + (font.getHeight() / 2) + 8;
					if (s.charAt(i) == 'D' || s.charAt(i) == 'H'
							|| s.charAt(i) == 'K' || s.charAt(i) == 'N'
							|| s.charAt(i) == '�' || s.charAt(i) == 'O'
							|| s.charAt(i) == 'Q')
						temp = temp + (font.getHeight() / 2) + 10;
					if (s.charAt(i) == 'w' || s.charAt(i) == 'M')
						temp = temp + (font.getHeight() / 2) + 14;
					if (s.charAt(i) == 'm') {
						if (font.getHeight() <= 40)
							temp = temp + (font.getHeight() / 2) + 15;
						else
							temp = temp + (font.getHeight() / 2) + 18;
					}
					if (s.charAt(i) == 'W') {
						if (font.getHeight() <= 40)
							temp = temp + (font.getHeight() / 2) + 17;
						else
							temp = temp + (font.getHeight() / 2) + 20;
					}
				}
			}

		}
		return temp;
	}

	static public String lowerCleanStringNS(String s) {
		// s = s.toLowerCase();
		s = s.replace('�', 'n');
		s = s.replace('�', 'a');
		s = s.replace('�', 'e');
		s = s.replace('�', 'i');
		s = s.replace('�', 'o');
		s = s.replace('�', 'u');

		s = s.replace('�', 'N');
		s = s.replace('�', 'A');
		s = s.replace('�', 'E');
		s = s.replace('�', 'I');
		s = s.replace('�', 'O');
		s = s.replace('�', 'U');

		return s;
	}

	static public String lowerCleanString(String s) {
		s = s.toLowerCase();
		s = s.replace('�', 'n');
		s = s.replace('�', 'a');
		s = s.replace('�', 'e');
		s = s.replace('�', 'i');
		s = s.replace('�', 'o');
		s = s.replace('�', 'u');
		String temp = null;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != ' ') {
				if (temp == null)
					temp = String.valueOf(s.charAt(i));
				else
					temp = temp + s.charAt(i);
			}

		}
		s = temp;
		return s;
	}



	public static Bitmap resizeBitmap(Bitmap image, int width, int height) {

		int rgb[] = null, rgb2[] = null;

		rgb = new int[image.getWidth() * image.getHeight()];

		image.getARGB(rgb, 0, image.getWidth(), 0, 0, image.getWidth(),
				image.getHeight());

		rgb2 = rescaleArray(rgb, image.getWidth(), image.getHeight(), width,
				height);

		Bitmap temp2 = new Bitmap(width, height);
		temp2.setARGB(rgb2, 0, width, 0, 0, width, height);

		return temp2;
	}

	private static int[] rescaleArray(int[] ini, int x, int y, int x2, int y2) {
		int out[] = new int[x2 * y2];
		for (int yy = 0; yy < y2; yy++) {
			int dy = yy * y / y2;
			for (int xx = 0; xx < x2; xx++) {
				int dx = xx * x / x2;
				out[(x2 * yy) + xx] = ini[(x * dy) + dx];
			}
		}
		return out;
	}

	public static EncodedImage scaleImageToWidth(EncodedImage encoded,
			int newWidth) {
		return scaleToFactor(encoded, encoded.getWidth(), newWidth);
	}

	public static EncodedImage scaleImageToHeight(EncodedImage encoded,
			int newHeight) {
		return scaleToFactor(encoded, encoded.getHeight(), newHeight);
	}

	public static EncodedImage scaleToFactor(EncodedImage encoded, int curSize,
			int newSize) {
		int numerator = Fixed32.toFP(curSize);
		int denominator = Fixed32.toFP(newSize);
		int scale = Fixed32.div(numerator, denominator);

		return encoded.scaleImage32(scale, scale);
	}

	public static String replace(String _text, String _searchStr,
			String _replacementStr) {
		// String buffer to store str
		StringBuffer sb = new StringBuffer();

		// Search for search
		int searchStringPos = _text.indexOf(_searchStr);
		int startPos = 0;
		int searchStringLength = _searchStr.length();

		// Iterate to add string
		while (searchStringPos != -1) {
			sb.append(_text.substring(startPos, searchStringPos)).append(
					_replacementStr);
			startPos = searchStringPos + searchStringLength;
			searchStringPos = _text.indexOf(_searchStr, startPos);
		}

		// Create string
		sb.append(_text.substring(startPos, _text.length()));

		return sb.toString();
	}

	public static String contruyeUrlDescarga(String _contexto,
			String[] _idDescargados, String _max, String _tipo) {
		StringBuffer url = new StringBuffer();
		// url.append(MainClass.urlGeneral);DESCOMENTAR LINEA
		url.append("Rokamservice.svc/" + _contexto);
		if (!_contexto.equals("eliminatorias")) {
			url.append("/lista=");
			if (_idDescargados != null) {
				for (int i = 0; i < _idDescargados.length; i++) {
					if (_idDescargados[i] != null)
						url.append(_idDescargados[i] + ",");
				}
				url.delete(url.length() - 1, url.length());
				if (url.toString().endsWith("null")) {
					url.delete(url.length() - 5, url.length());

				}
			} else
				url.append("0");

			url.append("/max=" + _max);
			// DESCOMENTAR FUNCION
			// if(_tipo==MainClass.IMEI){
			// url.append("/telefono="+_tipo);
			// }else if(_tipo!=null)
			// url.append("/tipo="+_tipo);

		} else {

			url.append("/semifinal=" + _tipo);
		}

		return url.toString();
	}

	public static String getMonth(String date) {

		String month = null;

		if (date.equals("01")) {
			month = "Ene";
		} else if (date.equals("02")) {
			month = "Feb";
		} else if (date.equals("03")) {
			month = "Mar";
		} else if (date.equals("04")) {
			month = "Abr";
		} else if (date.equals("05")) {
			month = "May";
		} else if (date.equals("06")) {
			month = "Jun";
		} else if (date.equals("07")) {
			month = "Jul";
		} else if (date.equals("08")) {
			month = "Ago";
		} else if (date.equals("09")) {
			month = "Sep";
		} else if (date.equals("10")) {
			month = "Oct";
		} else if (date.equals("11")) {
			month = "Nov";
		} else if (date.equals("12")) {
			month = "Dic";
		}

		return month;
	}
/*
	public static String obtenerTinyUrl(String url) throws IOException {
		String tiny = null;

		// url = MainClass.urlTiny + url;
		// //// //System.out.println("peticion tiny: "+url);
		HttpConnection conn = (HttpConnection) Connector.open(url + ";"
				+ MainClass.idealConnection);
		int progress = 0;
		InputStream in = null;
		byte[] data = new byte[256];
		try {
			in = conn.openInputStream();
			DataBuffer db = new DataBuffer();
			int chunk = 0;
			while (-1 != (chunk = in.read(data))) {
				progress += chunk;
				db.write(data, 0, chunk);
			}
			in.close();
			data = db.getArray();
		} catch (Exception e) {
		}

		if (data != null) {
			System.gc();

			StringBuffer sb = new StringBuffer();

			sb.append(new String(data, 0, data.length, "UTF-8"));

			tiny = new String(sb.toString());
			// //// //System.out.println("url return tiny"+tiny);
		}

		return tiny;
	}
*/
	public static String mergeStr(String first, String second) {
		System.out.println("1");
		String result = "";
		String other = Utils.reverse(second);
		System.out.println("2");

		result += (Integer.toString(other.length()).length() < 2) ? "0"
				+ other.length() : Integer.toString(other.length());
		System.out.println("3");

		String sub1 = first.substring(0, 19);
		String sub2 = first.substring(19, first.length());
		System.out.println("4");

		result += sub1;

		System.out.println("5");

		for (int i = 0; i < other.length(); i += 2) {
			int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);

			String next = other.substring(i, offset);

			result += next;

			result += sub2.substring(0, 2);

			sub2 = sub2.substring(2);

		}
		result += sub2;

		System.out.println("6");
		return result;
	}

	public static String sha1(String text) throws UnsupportedEncodingException {

		// System.out.println("ENTRO shaRes: " );
		SHA1Digest sha1Digest = new SHA1Digest();
		String sha = text;
		byte[] inpData = sha.getBytes("UTF-8");
		sha1Digest.update(inpData, 0, inpData.length);
		byte[] digest = sha1Digest.getDigest();
		StringBuffer shaRes = new StringBuffer(40); // 40 hex char is size of
													// 160-bit SHA-1 result
		for (int a = 0; a < digest.length; a++) {
			String tmp = Integer.toHexString(0xff & digest[a]);
			if (tmp.length() == 1) // If hex value is "0X" then tmp is just one
									// digit "X"
			{
				shaRes.append('0');
			}
			shaRes.append(tmp);
		}
		// System.out.println("shaRes: " + shaRes.toString());
		return shaRes.toString();

	}

	public static boolean isNumeric(String cadena) {

		try {

			// Integer.parseInt(cadena);
			char myArray[] = cadena.toCharArray();
			for (int i = 0; i < myArray.length; i++) {

				String num = String.valueOf(myArray[i]);

				Integer.parseInt(num);

			}
			// Long.parseLong(cadena);
			return true;

		} catch (NumberFormatException nfe) {

			return false;

		}

	}

	// public static String sha1(String s) {
	// if (s != null){
	// try {
	// SHA1Digest digest = new SHA1Digest();
	// byte[] bb = s.getBytes();
	// digest.update(bb, 0, bb.length);
	//
	// byte[] digestValue = new byte[digest.getDigestSize()];
	// digest.doFinal(digestValue, 0);
	//
	// return MD5.toHex(digestValue);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	//
	// return "";
	// }

	// private static boolean isFechaValida(String fechax) {
	// try {
	// SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy",
	// Locale.getDefault());
	// formatoFecha.setLenient(false);
	// formatoFecha.parse(fechax);
	// } catch (ParseException e) {
	// return false;
	// }
	//
	// return true;
	// }

	// date validation using SimpleDateFormat
	// it will take a string and make sure it's in the proper
	// format as defined by you, and it will also make sure that
	// it's a legal date
	// DateField dateField = new DateField()
	// public boolean isValidDate(String date)
	// {
	// // set date format, this can be changed to whatever format
	// // you want, MM-dd-yyyy, MM.dd.yyyy, dd.MM.yyyy etc.
	// // you can read more about it here:
	// // http://java.sun.com/j2se/1.4.2/docs/api/index.html
	//
	// SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	//
	// // declare and initialize testDate variable, this is what will hold
	// // our converted string
	//
	// Date testDate = null;
	//
	// // we will now try to parse the string into date form
	// try
	// {
	// testDate = sdf.parse(date);
	// }
	//
	// // if the format of the string provided doesn't match the format we
	// // declared in SimpleDateFormat() we will get an exception
	//
	// catch (ParseException e)
	// {
	// errorMessage = "the date you provided is in an invalid date" +
	// " format.";
	// return false;
	// }
	//
	// // dateformat.parse will accept any date as long as it's in the format
	// // you defined, it simply rolls dates over, for example, december 32
	// // becomes jan 1 and december 0 becomes november 30
	// // This statement will make sure that once the string
	// // has been checked for proper formatting that the date is still the
	// // date that was entered, if it's not, we assume that the date is invalid
	//
	// if (!sdf.format(testDate).equals(date))
	// {
	// errorMessage = "The date that you provided is invalid.";
	// return false;
	// }
	//
	// // if we make it to here without getting an error it is assumed that
	// // the date was a valid one and that it's in the proper format
	//
	// return true;
	//
	// } // end isValidDate

	public static String reverse(String chain) {
		String nuevo = "";

		for (int i = chain.length() - 1; i >= 0; i--) {
			nuevo += chain.charAt(i);
		}

		return nuevo;
	}

	public static String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}

	public static String getImei() {
		byte[] imei;
		try {
			imei = GPRSInfo.getIMEI();

			return GPRSInfo.imeiToString(imei, false);
		} catch (Exception e) {
			e.printStackTrace();

			int id = DeviceInfo.getDeviceId();
			return Integer.toString(id);
		}

	}

	public final static String replaceURL(String text, String searchString,
			String replacementString) {
		StringBuffer sBuffer = new StringBuffer();
		int pos = 0;

		while ((pos = text.indexOf(searchString)) != -1) {
			sBuffer.append(text.substring(0, pos));
			sBuffer.append(replacementString);
			text = text.substring(pos + searchString.length());
		}

		sBuffer.append(text);
		return sBuffer.toString();
	}
/*
	public static String getDeviceINFO() {
		try {
			String DeviceName = DeviceInfo.getDeviceName();
			System.out.println("DeviceName " + DeviceName);
			MainClass.MODELO = DeviceName;
			String SoftwareVersion = DeviceInfo.getSoftwareVersion();
			System.out.println("SoftwareVersion " + SoftwareVersion);
			MainClass.SOFTWARE = SoftwareVersion;
			String ManufacturerName = DeviceInfo.getManufacturerName();
			System.out.println("ManufacturerName " + ManufacturerName);
			MainClass.TIPO = ManufacturerName;

		} catch (Exception e) {

		}
		// DeviceInfo.
		return "";
	}
*/
	public static Bitmap resizeBitmap2(Bitmap image, int width, int height) {
		int imageWidth = image.getWidth();
		int imageHeight = image.getHeight();

		// Need an array (for RGB, with the size of original image)
		int rgb[] = new int[imageWidth * imageHeight];

		// Get the RGB array of image into "rgb"
		image.getARGB(rgb, 0, imageWidth, 0, 0, imageWidth, imageHeight);

		// Call to our function and obtain rgb2
		int rgb2[] = rescaleArray(rgb, imageWidth, imageHeight, width, height);

		// Create an image with that RGB array
		Bitmap temp2 = new Bitmap(width, height);

		temp2.setARGB(rgb2, 0, width, 0, 0, width, height);

		return temp2;
	}

	public static String swapFormat(String date) {
		String fechaArr[] = split(date, "-");
		String fecha = fechaArr[2] + "/" + fechaArr[1] + "/"
				+ fechaArr[0].substring(2);

		return fecha;

	}

	public static String[] split(String original, String separador) {
		Vector nodes = new Vector();
		String separator = separador; // "|";

		// Parse nodes into vector
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		// Get the last node
		nodes.addElement(original);

		// Create splitted string array
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);
				// System.out.println(result[loop]);
			}

		}

		return result;
	}

	public static String replaceConAcento(String text) {
		StringBuffer sBuffer = new StringBuffer();
		for (int i = 0; i < text.length(); i++) {
			if (text.charAt(i) == '�')
				sBuffer.append("&Ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&ntilde;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&aacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&eacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&iacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&oacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&Uacute;");
			else if (text.charAt(i) == '�')
				sBuffer.append("&uacute;");
			else
				sBuffer.append(text.charAt(i));
		}

		return sBuffer.toString();
	}

	public static String reemplazaHTML(String html) {
		String caracteres[] = { "&Aacute;", "&aacute;", "&Eacute;", "&eacute;",
				"&Iacute;", "&iacute;", "&Oacute;", "&oacute;", "&Uacute;",
				"&uacute;", "&Ntilde;", "&ntilde;" };
		String letras[] = { "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",
				"�", "�" };

		for (int counter = 0; counter < caracteres.length; counter++) {
			html = reemplaza(html, caracteres[counter], letras[counter]);
		}
		return html;
	}

	public static String reemplaza(String src, String orig, String nuevo) {
		if (src == null) {
			return null;
		}
		int tmp = 0;
		String srcnew = "";
		while (tmp >= 0) {
			tmp = src.indexOf(orig);
			if (tmp >= 0) {
				if (tmp > 0) {
					srcnew += src.substring(0, tmp);
				}
				srcnew += nuevo;
				src = src.substring(tmp + orig.length());
			}
		}
		srcnew += src;
		return srcnew;
	}

}
