package com.addcel.prosa.view.base.uicomponents;

import com.addcel.prosa.view.base.UtilIcon;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.component.ButtonField;



public class CustomImageButton extends Field {

	private int preferredHeight;
	private int preferredWidth;
	
	private String data;
	
	private Bitmap focus;
	private Bitmap unfocus;
	
	private boolean isFocus = false;
	
	public CustomImageButton(String nameBitmap) {
		
		super(ButtonField.CONSUME_CLICK|ButtonField.FIELD_HCENTER);

		isFocus = true;
		
		focus = UtilIcon.getBitMap(nameBitmap);
		
		preferredHeight = focus.getHeight();
		preferredWidth = focus.getWidth();
	}
	
	public CustomImageButton(String sFocus, String sUnfocus) {
		
		super(ButtonField.CONSUME_CLICK|ButtonField.FIELD_HCENTER);
		
		focus = UtilIcon.getBitMap(sFocus);
		unfocus = UtilIcon.getBitMap(sUnfocus);
		
		preferredHeight = focus.getHeight();
		preferredWidth = focus.getWidth();
	}
	
	public int getPreferredHeight() {
		return preferredHeight;
	}

	public int getPreferredWidth() {
		return preferredWidth;
	}

	protected void layout(int width, int height) {
		setExtent(preferredWidth, preferredHeight);
	}

	protected void paint(Graphics graphics) {

		if (isFocus){
			graphics.drawBitmap(0, 0, preferredWidth, preferredHeight, focus, 0, 0);
		} else {
			if (isFocus()){
				graphics.drawBitmap(0, 0, preferredWidth, preferredHeight, focus, 0, 0);
			} else {
				graphics.drawBitmap(0, 0, preferredWidth, preferredHeight, unfocus, 0, 0);
			}
		}
	}

	public boolean isFocusable() {
		return true;
	}
	
	public boolean isSelectable() {
		return true;
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		setEvent();
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			setEvent();
			return true;
		}
		return super.keyChar(character, status, time);
	}
	
	
	private void setEvent(){
		fieldChangeNotify(0);
	}	
}

