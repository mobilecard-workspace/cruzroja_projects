package com.addcel.prosa.view.base.uicomponents.vendedores;


import com.addcel.prosa.dto.Vendedor;
import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.RichTextField;

public class VendedorRichTextField extends RichTextField {
	
	public final static int NOMBRE = 0;
	public final static int LOGIN = 1;
	public final static int ROLE = 2;
	public final static int STATUS = 3;
	
	private String title;
	private String data;
	
	private int width;
	private int customWidth;
	
	public VendedorRichTextField(Vendedor vendedor, int option) {

		super("", RichTextField.NON_FOCUSABLE | RichTextField.USE_ALL_HEIGHT);

		width = Display.getWidth();
		customWidth = width/4;
		
		switch (option) {
		case NOMBRE:
			title = "Nombre: ";
			data = vendedor.getNombre();
			break;

		case LOGIN:
			title = "Login: ";
			data = vendedor.getLogin();
			break;

		case ROLE:
			title = "Rol: ";
			data = String.valueOf(vendedor.getRole());
			break;

		case STATUS:
			title = "Status: ";
			data = String.valueOf(vendedor.getStatus());
			break;
		}
	}


	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.LIST_DESCRIPTION_TITLE);
		graphics.drawText(title, 0, 0, DrawStyle.RIGHT, customWidth);
		graphics.setColor(UtilColor.LIST_DESCRIPTION_DATA);
		graphics.drawText(data, customWidth, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}







