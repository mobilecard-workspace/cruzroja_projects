package com.addcel.prosa.view.base;

import net.rim.device.api.ui.Color;

public class UtilColor {
	/*
	private final static int GREEN_01 = 0x90c1c7;
	private final static int GREEN_02 = 0x3db7b0;

	private final static int BLUE = 0x23486d;

	private final static int WHITE = Color.WHITE;
	private final static int RED = 0xc30042;
	
	private final static int GREY_01 = 0xececec;
	private final static int GREY_02 = 0xdfdfdf;
	private final static int GREY_03 = 0xb4bdb6;
	private final static int GREY_04 = 0x939598;
	private final static int GREY_05 = 0x231f20;
	*/
	
	private final static int GRIS = 0xdfdfdf;
	private final static int BLANCO = Color.WHITE;
	private final static int ROJO = 0xdc2b19;
	
	
	public final static int MAIN_BACKGROUND = BLANCO;

	public final static int TITLE_STRING = BLANCO;
	public final static int TITLE_BACKGROUND = ROJO;

	public final static int BUTTON_FOCUS = ROJO;
	public final static int BUTTON_SELECTED = GRIS;
	public final static int BUTTON_UNSELECTED = GRIS;

	public final static int BUTTON_STRING_FOCUS = BLANCO;
	public final static int BUTTON_STRING_SELECTED = ROJO;
	public final static int BUTTON_STRING_UNSELECTED = ROJO;

	public final static int SUBTITLE_STRING = BLANCO;
	public final static int SUBTITLE_BACKGROUND = ROJO;

	
	public final static int ELEMENT_STRING_CHOICE = ROJO;
	public final static int ELEMENT_STRING = ROJO;
	public final static int ELEMENT_BACKGROUND = BLANCO;

	public final static int EDIT_TEXT_DATA_FOCUS = ROJO;
	public final static int EDIT_TEXT_DATA_UNFOCUS = ROJO;
	
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = GRIS;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = GRIS;

	public final static int LIST_DESCRIPTION_TITLE = ROJO;
	public final static int LIST_DESCRIPTION_DATA = ROJO;

	public final static int LIST_BACKGROUND_SELECTED = BLANCO;
	public final static int LIST_BACKGROUND_UNSELECTED = ROJO;


	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}
