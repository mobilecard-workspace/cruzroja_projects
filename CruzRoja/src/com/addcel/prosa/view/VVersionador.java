package com.addcel.prosa.view;

import com.addcel.prosa.view.base.UtilColor;
import com.addcel.prosa.view.base.Viewable;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.TextField;
import net.rim.device.api.ui.container.HorizontalFieldManager;



public class VVersionador extends Viewable implements FieldChangeListener {

	public FontFamily alphaSansFamily = null;
	
	ButtonField descarga = null;
	ButtonField continuar = null;
	String url;
	
	public VVersionador(String url, boolean isRequired) {
		
		super(false, true, "header_apoya_");
		
		this.url = url;
		
		try {
			
			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			//LabelField importante = new LabelField("�IMPORTANTE!");
			
			LabelField importante = new LabelField("�IMPORTANTE!"){
				
				protected void paint(Graphics graphics){
					
					graphics.setColor(Color.RED);
					
					super.paint(graphics);
				}	
			};
			
			importante.setFont(alphaSansFamily.getFont(Font.PLAIN, 9, Ui.UNITS_pt));
			add(importante);
			//TextField nueva = new TextField(TextField.NON_FOCUSABLE);
			
			TextField nueva = new TextField(TextField.NON_FOCUSABLE){
				
				protected void paint(Graphics graphics){
					
					graphics.setColor(Color.RED);
					
					super.paint(graphics);
				}	
			};
			
			nueva.setText("Hay una nueva versi�n de Cruz Roja Donativos disponible.");
			nueva.setFont(alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt));
			add(nueva);
			
			
			if (isRequired){
				
				descarga = new ButtonField("Descargar", ButtonField.CONSUME_CLICK);
				descarga.setChangeListener(this);
				add(new NullField());
				add(descarga);
				
			} else {
				
				
				descarga = new ButtonField("Descargar", ButtonField.CONSUME_CLICK);
				descarga.setChangeListener(this);
				
				continuar = new ButtonField("Continuar", ButtonField.CONSUME_CLICK);
				continuar.setChangeListener(this);
				
				add(new NullField());

				addElements(descarga, continuar);
			}
			
			TextField aclaracion = new TextField(TextField.READONLY|TextField.NON_FOCUSABLE){
				
				protected void paint(Graphics graphics){
					
					graphics.setColor(Color.RED);
					
					super.paint(graphics);
				}	
			};
			aclaracion.setText(					
					"Dudas o aclaraciones cont�ctanos a soporte@addcel.com " +
					"o al t�lefono en el D.F. y �rea metropolitana 5540 3124 ");
			
			aclaracion.setFont(alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
			
			add(aclaracion);
			
		} catch (ClassNotFoundException e) {
			Dialog.alert("El sistema no pudo configurarse para ejecutarse.");
			e.printStackTrace();
		}
	}

	public void fieldChanged(Field field, int context) {
		
		if (field == descarga){
			
			execute(url);
		} else if (field == continuar){
			
			UiApplication.getUiApplication().popScreen(this);
			UiApplication.getUiApplication().pushScreen(new VConfiguracionDatos());
		}
	}

	private boolean execute(String url){
		BrowserSession browserSession; 
		browserSession = Browser.getDefaultSession();
		
		browserSession.displayPage(url);
		return true;
	}
	
	
	private void addElements(Field element1, Field element2){

		HorizontalFieldManager manager = new HorizontalFieldManager(HorizontalFieldManager.FIELD_HCENTER);
		manager.add(element1);
		manager.add(element2);
		add(manager);
	}
	
	protected void analyzeData(int request, Object object) {
	}

	protected void doFieldChanged(Field field, int context) {
	}
}