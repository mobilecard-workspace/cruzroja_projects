package com.addcel.prosa.view.back;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.NullField;

import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VSumateResultado extends Viewable implements FieldChangeListener{
	
	private ElementLabelField conceptoTxt;
	private ElementLabelField montoTxt;
	private ElementLabelField tdcTxt;	
	private ElementLabelField referenciaTxt;
	
	private ElementLabelField conceptoEdit;
	private ElementLabelField montoEdit;
	private ElementLabelField tdcEdit;
	private ElementLabelField referenciaEdit;
	
	private CustomSelectedSizeButton salir;
	
	PagoRespuesta pagoRespuesta;
	
	public VSumateResultado(boolean isSetTitle, String title,
			PagoRespuesta pagoRespuesta) {
		super(isSetTitle, title);

		this.pagoRespuesta = pagoRespuesta;
		
		conceptoTxt = new ElementLabelField("Concepto:");
		montoTxt = new ElementLabelField("Monto:");
		tdcTxt = new ElementLabelField("TDC:");
		referenciaTxt = new ElementLabelField("Referencia:");
		
		if (pagoRespuesta.getResultado() == 1) {
			conceptoEdit = new ElementLabelField(pagoRespuesta.getTipoDonacion());
		} else {
			conceptoEdit = new ElementLabelField(pagoRespuesta.getMensajeError());
		}

		montoEdit = new ElementLabelField(String.valueOf(pagoRespuesta.getMonto()));
		tdcEdit = new ElementLabelField(String.valueOf(pagoRespuesta.getTdcM()));
		referenciaEdit = new ElementLabelField(pagoRespuesta.getAddcelR());
		
		add(new NullField());
		add(conceptoTxt);
		add(conceptoEdit);

		
		if (pagoRespuesta.getResultado() == 1) {
			add(montoTxt);
			add(montoEdit);
		}
		

		//add(montoTxt);
		//add(montoEdit);
		add(tdcTxt);
		add(tdcEdit);
		add(referenciaTxt);
		add(referenciaEdit);

		if (pagoRespuesta.getResultado() == 1) {
			salir = new CustomSelectedSizeButton("Aceptar", 1);

		} else {
			salir = new CustomSelectedSizeButton("Intentar de nuevo", 1);
		}

		salir.setChangeListener(this);
		add(salir);
	}

	public void fieldChanged(Field field, int context) {
		
		if (field == salir){
			
			if (pagoRespuesta.getResultado() == 1){
				System.exit(0);
			} else {
				UiApplication.getUiApplication().popScreen(this);
			}
			
		}
	}

	protected void analyzeData(int request, Object object) {
	}
}
