package com.addcel.prosa.view;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.apiold.dto.UserBean;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Desastre;
import com.addcel.prosa.dto.Estado;
import com.addcel.prosa.dto.Monto;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.model.token.DAPagoTarjetaAddcel;
import com.addcel.prosa.view.back.VSumateResultado;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomImageButton;
import com.addcel.prosa.view.base.uicomponents.CustomPasswordEditField;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VDatosDonadorMB extends Viewable implements FieldChangeListener{

	private ElementLabelField montoTxt;
	private ElementLabelField cvv2Txt;
	private ElementLabelField passwordTxt;
	
	//private CustomObjectChoiceField montoChoice;
	
	private CustomEditField montoEdit;
	private CustomEditField cvv2Edit;
	private CustomPasswordEditField passwordEdit;
	
	private CustomImageButton donar;
	
	private int tarjetaCredito = 0;
	//private String usuario;
	//private String password;
	
	private Object destinoDonacion;
	private Monto monto;
	
	//public VDatosDonadorMB(boolean isSetTitle, String title, String usuario, String password) {
	public VDatosDonadorMB(Object destinoDonacion, Monto monto) {
		
		super(false, true, "header_rdonativo_");

		this.destinoDonacion = destinoDonacion;
		this.monto = monto;
		
		montoTxt = new ElementLabelField("Donaci�n:");
		cvv2Txt = new ElementLabelField("Cvv2");
		passwordTxt = new ElementLabelField("Contrase�a");
		//montoChoice = new CustomObjectChoiceField("Monto:", null, 0);
		//montoChoice.setChangeListener(this);
		montoEdit = new CustomEditField(String.valueOf(monto.getMonto()), 100, EditField.FILTER_NUMERIC);
		cvv2Edit = new CustomEditField(3, EditField.FILTER_INTEGER);
		passwordEdit = new CustomPasswordEditField(12, EditField.CONSUME_INPUT);
		donar = new CustomImageButton("btn_donaro_", "btn_donar_");
		donar.setChangeListener(this);

		add(new LabelField());
		//add(montoChoice);
		add(new LabelField());
		add(montoTxt);
		add(montoEdit);
		add(new LabelField());
		add(cvv2Txt);
		add(cvv2Edit);
		add(new LabelField());
		add(passwordTxt);
		add(passwordEdit);
		add(new LabelField());
		add(donar);
	}

	
	public void fieldChanged(Field arg0, int arg1) {
/*
		if (arg0 == montoChoice){
			
			if (ObjectChoiceField.CONTEXT_CHANGE_OPTION == arg1){
				
				montoEdit.setText("");
			}

		} else 
			
*/
			
			if (arg0 == donar) {

			if (checarCampos()) {

				JSONObject jsonObject = new JSONObject();
				JSONObject jsonObject1 = new JSONObject();

				try {

					
					String producto = String.valueOf(monto.getIdMonto());
					
					if (destinoDonacion instanceof Desastre){
						
						Desastre desastre = (Desastre)destinoDonacion;

						jsonObject.put("idDesastre", desastre.getIdDesastre());
					} else if (destinoDonacion instanceof Estado){
						
						Estado estado = (Estado)destinoDonacion;

						jsonObject.put("idEstado", estado.getIdEstado());
					}
					jsonObject.put("monto", String.valueOf(monto.getMonto()));
					jsonObject.put("producto", producto);
					jsonObject.put("login", UserBean.nameLogin);
					jsonObject.put("password", UserBean.passlogin);
					jsonObject.put("cvv2", cvv2Edit.getText());
					//jsonObject.put("idProveedor", Start.ID_PROVEEDOR);
					//jsonObject.put("tipoTarjeta", String.valueOf(tarjetaCredito));
	
					jsonObject1.put("idProveedor", Start.ID_PROVEEDOR);
					jsonObject1.put("usuario", "userPrueba");
					jsonObject1.put("password", "passwordPrueba");

					DAPagoTarjetaAddcel pagoTarjeta = new DAPagoTarjetaAddcel(this, jsonObject1.toString());
					pagoTarjeta.setJson(jsonObject);
					Thread thread = new Thread(pagoTarjeta);
					thread.start();

				} catch (JSONException e) {
					this.analyzeData(DataAccessible.ERROR, Error.JSON_EXCEPTION);
					e.printStackTrace();
				}

			} else {
				this.analyzeData(DataAccessible.ERROR, "Existen campos sin informaci�n.");
			}
		}
	}

	protected void analyzeData(int request, Object object) {
		
		if (request == DataAccessible.DATA){
			
			if (object instanceof Vector){
				
				Vector montos = (Vector)object;
				int size = montos.size();
				Monto catmontos[] = new Monto[size];
				montos.copyInto(catmontos);
				//montoChoice.setChoices(catmontos);
			} else if(object instanceof PagoRespuesta){
				
				UiApplication.getUiApplication().pushScreen(new VSumateResultado(true, "", (PagoRespuesta)object));
			}
			
		} else if (object instanceof PagoRespuesta){
			
			PagoRespuesta pagoRespuesta = (PagoRespuesta)object;
			
			String mensajeError = pagoRespuesta.getMensajeError();
			
			if ((mensajeError != null)&&(mensajeError.length() > 0)){
				
				Dialog.alert(pagoRespuesta.getMensajeError());
			}
			
			
		} else if (request == DataAccessible.ERROR){
			
			Dialog.alert((String)object);
		}
	}
	

	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(cvv2Edit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(CustomEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
}
