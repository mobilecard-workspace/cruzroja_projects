package com.addcel.prosa.view;

import java.util.Timer;
import java.util.TimerTask;

import org.json.me.JSONObject;

import com.addcel.api.dataaccess.DAVersionamiento;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.UtilIcon;
import com.addcel.prosa.view.base.Viewable;


import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;

public class VSplash extends Viewable {

	private static final int VERSIONAMIENTO = 0;
	
	public VSplash() {

		super(false, null);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		DAVersionamiento daVersionamiento = new DAVersionamiento(this, URL.URL_VERSIONADOR, null, VERSIONAMIENTO);
		Thread thread = new Thread(daVersionamiento);
		thread.start();
	}
	
	
	protected void analyzeData(int request, Object object) {
		
		String error = null;
		
		switch(request){
		
		case VERSIONAMIENTO:

			verificarVersionamiento(object);
			//UiApplication.getUiApplication().popScreen(this);
			//UiApplication.getUiApplication().pushScreen(new InicioSesion());
			break;
		
		case DataAccessible.NUM_ERROR_IO:
			error = (String)object;
			Dialog.alert(error);
			close();
			break;
			
		case DataAccessible.NUM_ERROR_JSON:
			error = (String)object;
			Dialog.alert(error);
			close();
			break;
		
		}
	}
	
	
	private void verificarVersionamiento(Object object){
		
		JSONObject jsonObject = (JSONObject)object;
		
		// {"id":44,"version":"1.0.0","url":"http://www.mobilecard.mx","tipo":"1"}
		//{"mensaje":"Error","id":-1}
		
		int id = jsonObject.optInt("id");
		
		if (id < 0){
			
			Dialog.alert("No se obtuvo información del versionamiento.");
			
		} else {

			String version = jsonObject.optString("version", null); 
			String tipo = jsonObject.optString("tipo", null);
			//String tipo = "2";
			String url = jsonObject.optString("url", null);
			
			ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();
			
			if (version.equals(appDesc.getVersion())) {
				UiApplication.getUiApplication().popScreen(this);
				UiApplication.getUiApplication().pushScreen(new VConfiguracionDatos());
			} else {

				if (tipo.equals("1")) {
					//obligatoria
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new VVersionador(url, true));
				} else if (tipo.equals("2")) {
					//no obligatoria
					UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
					UiApplication.getUiApplication().pushScreen(new VVersionador(url, false));
				}
			}
		}
	}
}



/*
public class VSplash extends Viewable {

	public VSplash() {

		super(false, null);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 4000);
	}

	class Wait extends TimerTask {

		private VSplash viewSplash;

		public Wait(VSplash viewSplash) {

			this.viewSplash = viewSplash;
		}

		public void run() {

			UiApplication.getUiApplication().invokeAndWait(new Runnable() {

				public void run() {
					UiApplication.getUiApplication().popScreen(viewSplash);
					//UiApplication.getUiApplication().pushScreen(new VLogin(true, "Acceso"));
					UiApplication.getUiApplication().pushScreen(new VConfiguracionDatos());
					
				}
			});
		}
	}

	protected void analyzeData(int request, Object object) {
	}
}
*/
