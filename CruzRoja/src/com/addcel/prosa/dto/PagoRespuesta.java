package com.addcel.prosa.dto;

/*

{
	"tipoDonacion":"Donaci�n por  Estado",
	"monto":"------",
	"tdcM":"xxxx-xxxx-xxxx-3456",
	"autorizacion":"000000",
	"addcelR":"------",
	"resultado":0,
	"mensajeError":"Ocurrio un error durante el pago en la app."
}

*/

public class PagoRespuesta {

	private String tipoDonacion;
	private String monto;
	private String tdcM;
	private String autorizacion;
	private String addcelR;
	private String mensajeError;
	private int resultado;
	
	public String getTipoDonacion() {
		return tipoDonacion;
	}
	public void setTipoDonacion(String tipoDonacion) {
		this.tipoDonacion = tipoDonacion;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getTdcM() {
		return tdcM;
	}
	public void setTdcM(String tdcM) {
		this.tdcM = tdcM;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getAddcelR() {
		return addcelR;
	}
	public void setAddcelR(String addcelR) {
		this.addcelR = addcelR;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public int getResultado() {
		return resultado;
	}
	public void setResultado(int resultado) {
		this.resultado = resultado;
	}
}


/*
{
	"idError":0,
	"mensajeError":"El pago fue exitoso.",
	"referencia":101,
	"autorizacion":16094
}

*/