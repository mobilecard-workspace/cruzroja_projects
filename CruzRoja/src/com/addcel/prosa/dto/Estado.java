package com.addcel.prosa.dto;

/*

{
	"estados": 
	[
		{
			"idEstado": "-1",
			"descripcion": "Selecciona ..."
		},
		{
			"idEstado": "99",
			"descripcion": "Sede Nacional"
		},
		{
			"idEstado": "1",
			"descripcion": "Aguascalientes"
		}
	]
}

*/

public class Estado {

	private int idEstado;
	private String descripcion;
	
	public int getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}
	
	public void setIdEstado(String idEstado) {
		this.idEstado = Integer.parseInt(idEstado);
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String toString(){
		return descripcion;
	}
}
