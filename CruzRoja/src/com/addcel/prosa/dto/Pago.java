package com.addcel.prosa.dto;

public class Pago {

	/*
	{
	"email":"prueba@addcel.com",
	"nombre":"NombreCompletoApppellidos",
	"idMonto":1,
	"monto":100,
	"tipoTarjeta":1,
	"tarjeta":"123455566666666",
	"cvv2":"5555",
	"vigencia":"01/15", 
	"idProveedor":"1",
	"token":"Je1L2NY2mNHC/aJFleOpIB4V+18r9d3mbo1PK6MTiz8=", 
	"imei":"189A7C55-8B44-48DB-A987-107595595F28",
	"modelo":"iPad",
	"software":"7.0"
	}
	*/
	
	private String email;
	private String nombre;
	private String idMonto;
	private String monto;
	private String tipoTarjeta;
	private String tarjeta;
	private String cvv2;
	private String vigencia;
	private String idProveedor;
	private String token;
	private String imei;
	private String modelo;
	private String software;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIdMonto() {
		return idMonto;
	}
	public void setIdMonto(String idMonto) {
		this.idMonto = idMonto;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getSoftware() {
		return software;
	}
	public void setSoftware(String software) {
		this.software = software;
	}
}
