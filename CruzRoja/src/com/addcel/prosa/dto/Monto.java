package com.addcel.prosa.dto;

public class Monto {

/*
	
{
	"montos": [
		{
			"idMonto": 8,
			"monto": 50
		},
		{
			"idMonto": 12,
			"monto": 1000
		}
	],
	"montoMaximo": "5000"
}
	
*/
	private int idMonto;
	private String monto;
	
	public int getIdMonto() {
		return idMonto;
	}
	public void setIdMonto(int idMonto) {
		this.idMonto = idMonto;
	}
	public void setIdMonto(String sMonto) {
		this.idMonto = Integer.parseInt(sMonto);
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String toString(){
		return monto;
	}
}
