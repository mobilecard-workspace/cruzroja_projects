package com.addcel.prosa.dto;

/*

{
	"desastres": [
	
		{
			"idDesastre": "-1",
			"descripcion": "Selecciona ..."
		},
		{
			"idDesastre": "1",
			"descripcion": "Desastre 1"
		}
		
	]
}

*/

public class Desastre {

	private int idDesastre;
	private String descripcion;
	
	public int getIdDesastre() {
		return idDesastre;
	}
	public void setIdDesastre(int idDesastre) {
		this.idDesastre = idDesastre;
	}
	public void setIdDesastre(String idDesastre) {
		this.idDesastre = Integer.parseInt(idDesastre);
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String toString(){
		return descripcion;
	}
}
