package com.addcel.prosa.model;

public class URL {

	//public static String URL = "http://50.57.192.213:8080";
	public static String URL = "http://www.mobilecard.mx:8080";
	
	public static String URL_VERSIONADOR = "http://mobilecard.mx:8080/Vitamedica/getVersion?idApp=7&idPlataforma=3";
	
	public static String URL_GET_TOKEN = URL + "/CruzRojaServicios/getToken";
	public static String URL_GET_CATALOGO_MONTOS = URL + "/CruzRojaServicios/getMontos";
	public static String URL_GET_PAGOS = URL + "/CruzRojaServicios/pago-visa_";
	public static String URL_GET_PAGOS_ADDCEL = URL + "/CruzRojaServicios/pago-visa";
	public static String URL_LOGIN = URL + "/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_UPDATE_PASS_MAIL = URL + "/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	
	public static String URL_ESTADOS = URL + "/CruzRojaServicios/catalogoEstados";
	public static String URL_DESASTRES = URL + "/CruzRojaServicios/consultaDesastres";
	public static String URL_MONTOS = URL + "/CruzRojaServicios/getMontos";
}
