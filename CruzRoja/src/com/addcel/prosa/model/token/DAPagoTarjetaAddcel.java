package com.addcel.prosa.model.token;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.implementation.connect.MethodGET;
import com.addcel.api.dataaccess.components.implementation.crypto.DescryptHard;
import com.addcel.api.dataaccess.components.implementation.crypto.EncryptHard;
import com.addcel.api.dataaccess.components.implementation.crypto.EncryptSensitive;
import com.addcel.api.util.UtilBB;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.dto.Token;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.token.object.OPagoRespuesta;
import com.addcel.prosa.model.token.object.OToken;
import com.addcel.prosa.view.base.Viewable;

public class DAPagoTarjetaAddcel extends DataAccessible implements Runnable {

	private JSONObject jsonObject;
	//private String monto;

	public DAPagoTarjetaAddcel(Viewable viewable, String data) {
		super(viewable, data);
		//this.monto = monto;
	}

	public void setJson(JSONObject jsonObject) {

		this.jsonObject = jsonObject;
	}

	public void run() {
		execute(viewable, data);
	}

	public void execute(Viewable viewable, String data) {

		try {

			this.encryptionable = new EncryptHard();
			
			String encrypt = encryptionable.execute(data);

			connectable = new MethodGET(URL.URL_GET_TOKEN);
			connectable.execute(encrypt);

			String descrypt = (String) connectable.getData();

			this.descryptionable = new DescryptHard();
			descrypt = descryptionable.execute(descrypt);

			
			this.objectable = new OToken();
			objectable.execute(descrypt);
			Token token = (Token) objectable.getData();

			if (token.getIdError() > 0) {

				viewable.setData(DataAccessible.ERROR, token.getMensajeError());
			} else {

				jsonObject.put("token", token.getToken());
				jsonObject.put("imei", UtilBB.getImei());
				jsonObject.put("cx", "0.0");
				jsonObject.put("cy", "0.0");
				jsonObject.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
				jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
				jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
				jsonObject.put("key", UtilBB.getImei());
				
				encryptionable = new EncryptSensitive();
				encrypt = encryptionable.execute(jsonObject.toString());

				connectable = new MethodGET(URL.URL_GET_PAGOS_ADDCEL);
				connectable.execute(encrypt);
				String info = (String) connectable.getData();
				String info2 = descryptionable.execute(info);

				OPagoRespuesta oPagoRespuesta = new OPagoRespuesta();
				oPagoRespuesta.execute(info2);
				PagoRespuesta pagoRespuesta = (PagoRespuesta) oPagoRespuesta.getData();

				viewable.setData(DataAccessible.DATA, pagoRespuesta);
			}

		} catch (JSONException e) {
			e.printStackTrace();
			viewable.setData(DataAccessible.ERROR, e.toString());
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}
