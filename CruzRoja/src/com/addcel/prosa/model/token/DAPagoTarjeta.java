package com.addcel.prosa.model.token;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.implementation.connect.MethodGET;
import com.addcel.api.dataaccess.components.implementation.crypto.DescryptHard;
import com.addcel.api.dataaccess.components.implementation.crypto.EncryptHard;
import com.addcel.api.dataaccess.components.implementation.crypto.EncryptSensitive;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.dto.Token;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.token.object.OPagoRespuesta;
import com.addcel.prosa.model.token.object.OToken;
import com.addcel.prosa.view.base.Viewable;

public class DAPagoTarjeta extends DataAccessible implements Runnable {

	
	private JSONObject jsonObject;
	private String monto;
	
	public DAPagoTarjeta(Viewable viewable, String data, String monto) {
		super(viewable, data);
		this.monto = monto;
	}

	
	public void setJson(JSONObject jsonObject){
		
		this.jsonObject = jsonObject;
	}
	
	public void run() {
		execute(viewable, data);
	}

	public void execute(Viewable viewable, String data) {

		try {

			this.encryptionable = new EncryptHard();
			String encrypt = encryptionable.execute(data);
			
			connectable = new MethodGET(URL.URL_GET_TOKEN);
			connectable.execute(encrypt);
			
			String descrypt = (String)connectable.getData();
			

			this.descryptionable = new DescryptHard();
			descrypt = descryptionable.execute(descrypt);
			
			
			this.objectable = new OToken();
			objectable.execute(descrypt);
			Token token = (Token)objectable.getData();
			
			
			if (token.getIdError() > 0){
				
				viewable.setData(DataAccessible.ERROR, token.getMensajeError());
			} else {

				try {
					
					jsonObject.put("token", token.getToken());
					//jsonObject.put("imei", UtilBB.getImei());
					//jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
					//jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));

					encryptionable = new EncryptSensitive();
					encrypt = encryptionable.execute(jsonObject.toString()); 

					connectable = new MethodGET(URL.URL_GET_PAGOS);
					connectable.execute(encrypt);
					String info = (String)connectable.getData();
					
					descryptionable = new DescryptHard();
					String info2 = descryptionable.execute(info);
					
					OPagoRespuesta oPagoRespuesta = new OPagoRespuesta();
					oPagoRespuesta.execute(info2);
					PagoRespuesta pagoRespuesta = (PagoRespuesta)oPagoRespuesta.getData();
					pagoRespuesta.setMonto(monto);
					
					viewable.setData(DataAccessible.DATA, pagoRespuesta);
					
				} catch (JSONException e) {
					e.printStackTrace();
					throw new OwnException(Error.JSON_EXCEPTION);
				}
			}

		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
}
