package com.addcel.prosa.model.token.object;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Desastre;
import com.addcel.prosa.dto.Monto;

public class ODesastres implements Objectable{

	
	private Vector desastres;
	
	public void execute(String json) throws OwnException {
		
		try {
			
			JSONObject jsonObjects = new JSONObject(json);
			
			JSONArray jsonArray = jsonObjects.optJSONArray("desastres");
					
			int length = jsonArray.length();
			
			desastres = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = jsonArray.optJSONObject(index);
				
				Desastre desastre = new Desastre();
				
				desastre.setIdDesastre(jsonObject.optString("idDesastre"));
				desastre.setDescripcion(jsonObject.optString("descripcion"));
				
				desastres.addElement(desastre);				
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {
		return desastres;
	}

}
