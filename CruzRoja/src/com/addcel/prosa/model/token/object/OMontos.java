package com.addcel.prosa.model.token.object;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Monto;

public class OMontos implements Objectable{

	private Vector catalogoMontos;
	private String montoMaximo;
	
	public void execute(String json) throws OwnException {
		
		try {
			
			JSONObject jsonObjects = new JSONObject(json);
			
			montoMaximo = jsonObjects.optString("montoMaximo", null);
			
			JSONArray jsonArray = jsonObjects.optJSONArray("montos");
					
			int length = jsonArray.length();
			
			catalogoMontos = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = jsonArray.optJSONObject(index);
				
				Monto monto = new Monto();
				
				monto.setIdMonto(jsonObject.optString("idMonto"));
				monto.setMonto(jsonObject.optString("monto"));
				
				catalogoMontos.addElement(monto);				
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {
		return catalogoMontos;
	}

	
	public String getMontoMaximo() {
		return montoMaximo;
	}
}
