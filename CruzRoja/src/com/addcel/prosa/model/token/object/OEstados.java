package com.addcel.prosa.model.token.object;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Estado;

public class OEstados implements Objectable{

	
	private Vector estados;
	
	public void execute(String json) throws OwnException {
		
		try {
			
			JSONObject jsonObjects = new JSONObject(json);
			
			JSONArray jsonArray = jsonObjects.optJSONArray("estados");
					
			int length = jsonArray.length();
			
			estados = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = jsonArray.optJSONObject(index);
				
				Estado estado = new Estado();
				
				estado.setIdEstado(jsonObject.optString("idEstado"));
				estado.setDescripcion(jsonObject.optString("descripcion"));
				
				estados.addElement(estado);				
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {
		return estados;
	}

}