package com.addcel.prosa.model.token;

import java.util.Hashtable;
import java.util.Vector;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.implementation.connect.MethodGET;
import com.addcel.api.dataaccess.components.implementation.crypto.DescryptHard;
import com.addcel.api.dataaccess.components.implementation.crypto.EncryptHard;
import com.addcel.prosa.Start;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.token.object.ODesastres;
import com.addcel.prosa.model.token.object.OEstados;
import com.addcel.prosa.model.token.object.OMontos;
import com.addcel.prosa.view.base.Viewable;

public class DACatalogos extends DataAccessible implements Runnable {

	private Viewable viewable;
	private String data;
	private String montoMaximo;
	private Hashtable elements;
	
	public DACatalogos(Viewable viewable, String data) {
		super(viewable, data);

		this.viewable = viewable;
		this.data = data;
	}

	public void run() {

		execute(viewable, data);
	}

	protected void execute(Viewable viewable, String json) {
		Vector estados;
		Vector desastres;
		Vector montos;
		
		try {
			
			estados = jsonEstados();
			desastres = jsonDesastres();
			montos = jsonMontos();
			
			elements = new Hashtable();
			
			elements.put("estados", estados);
			elements.put("desastres", desastres);
			elements.put("montos", montos);
			elements.put("montoMaximo", montoMaximo);
			
			viewable.setData(DataAccessible.DATA, elements);
			
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		}
	}
	
	
	private Vector jsonEstados() throws OwnException {

		connectable = new MethodGET(URL.URL_ESTADOS);
		connectable.execute(null);
		String info = (String) connectable.getData();

		descryptionable = new DescryptHard();
		info = descryptionable.execute(info);
		System.out.println(info);
		
		
		OEstados oEstados = new OEstados();
		oEstados.execute(info);
		
		Vector estados = (Vector)oEstados.getData();
		
		return estados;
	}

	private Vector jsonMontos() throws OwnException {

		//String json = "{\"proveedor\":\"21\"}";

		String json = "{\"proveedor\":\"" + Start.ID_PROVEEDOR + "\"}";
		
		this.encryptionable = new EncryptHard();
		String encrypt = encryptionable.execute(json);

		connectable.setUrl(URL.URL_GET_CATALOGO_MONTOS);
		connectable.execute(encrypt);
		String descrypt = (String) connectable.getData();

		this.descryptionable = new DescryptHard();
		descrypt = descryptionable.execute(descrypt);

		System.out.println(descrypt);
		
		OMontos catalogoMontos = new OMontos();
		catalogoMontos.execute(descrypt);
		
		montoMaximo = catalogoMontos.getMontoMaximo();
		Vector montos = (Vector)catalogoMontos.getData();

		return montos;
	}

	private Vector jsonDesastres() throws OwnException {

		connectable.setUrl(URL.URL_DESASTRES);
		connectable.execute(null);
		String info = (String) connectable.getData();

		info = descryptionable.execute(info);
		System.out.println(info);
		
		ODesastres catalogoDesastres = new ODesastres();
		catalogoDesastres.execute(info);
		
		Vector desastres = (Vector)catalogoDesastres.getData();
		
		return desastres;
	}
}
