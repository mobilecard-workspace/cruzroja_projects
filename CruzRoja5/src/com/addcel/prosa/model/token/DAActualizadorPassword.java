package com.addcel.prosa.model.token;

import java.io.UnsupportedEncodingException;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.UtilSecurity;
import com.addcel.prosa.dto.Token;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.token.encrypt.DescryptToken;
import com.addcel.prosa.model.token.encrypt.EncryptToken;
import com.addcel.prosa.model.token.object.OToken;
import com.addcel.prosa.view.base.Viewable;

public class DAActualizadorPassword extends DataAccessible implements Runnable {

	
	private String user;
	private String password; 
	private String newPassword;
	private String jsonEncripter;
	public DAActualizadorPassword(Viewable viewable, String data) {
		super(viewable, data);
		
		
		
		
		
	}
	
	public DAActualizadorPassword(String user, String password, String newPassword) {
		super(null, null);
		
		this.user = user;
		this.password = password;
		this.newPassword = newPassword;
		
		jsonEncripter = securityPasswordJson(user, password, newPassword);
	}
	
	
	public void run() {
		
		execute(null, jsonEncripter);
	}
	

	protected void execute(Viewable viewable, String json) {
		
		try {
			
			connectable = new MethodGET(URL.URL_UPDATE_PASS_MAIL);
			connectable.execute(json);
			
			String descrypt = (String) connectable.getData();

			descrypt = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(newPassword), descrypt);
			System.out.println(descrypt);
			
			
		} catch (OwnException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	public String securityPasswordJson(String user, String password, String newPassword){
		
		String pas = null;
		String pass = null;
		
		try {
			
			pas = UtilSecurity.aesEncrypt(UtilSecurity.parsePass(newPassword), updatePswd(user, password, newPassword));
			pass = UtilSecurity.mergeStr(pas, newPassword);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return pass;
	}
	
	

	public String updatePswd(String login, String password, String newPassword) throws UnsupportedEncodingException {

		StringBuffer jsonPswd = new StringBuffer("{\"login\":\"");
		jsonPswd.append(login);
		jsonPswd.append("\",\"passwordS\":\""+ UtilSecurity.sha1(password));
		jsonPswd.append("\",\"password\":\"" + newPassword
				+ "\",\"newPassword\":\"");
		jsonPswd.append(newPassword + "\"}");
		
		System.out.println(jsonPswd.toString());
		
		return jsonPswd.toString();
	}
}
