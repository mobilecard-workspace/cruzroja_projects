package com.addcel.prosa.model.token;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.model.versionador.VVersionador;
import com.addcel.prosa.view.base.Viewable;

public class DAVersionador extends DataAccessible implements Runnable {

	public DAVersionador(Viewable viewable, String data) {
		super(viewable, data);
		// TODO Auto-generated constructor stub
	}

	public void run() {
		execute(viewable, data);
		
	}

	protected void execute(Viewable viewable, String json) {
		
		try {
			
			connectable = new MethodGET(URL.URL_VERSIONADOR);
			connectable.execute(null);
			String info = (String) connectable.getData();
			
			System.out.println(info);

			json = new String(info);
			JSONObject jsonObject;
			jsonObject = new JSONObject(json);

			String version01 = jsonObject.optString("version", ""); 
			final String tipo = jsonObject.optString("tipo", "");
			final String url = jsonObject.optString("url", "");

			synchronized (Application.getEventLock()) {
				if (version01 != null && !version01.equals("")) {

					ApplicationDescriptor appDesc = ApplicationDescriptor.currentApplicationDescriptor();

					if (version01.equals(appDesc.getVersion())) {
						UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						UiApplication.getUiApplication().pushScreen(viewable);
					} else {

						if (tipo.equals("1")) {
							//obligatoria
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
							UiApplication.getUiApplication().pushScreen(new VVersionador("", url));
						} else if (tipo.equals("2")) {
							//no obligatoria
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
							//UiApplication.getUiApplication().pushScreen(new VInicio());
							UiApplication.getUiApplication().pushScreen(new VVersionador("", url));
						}
					}
				}
			}
			
			
			
/*
			descryptionable = new DescryptToken();
			descryptionable.execute(info);
			info = (String) descryptionable.getData();
			System.out.println(info);
			
			
			OEstados oEstados = new OEstados();
			oEstados.execute(info);
			
			Vector estados = (Vector)oEstados.getData();
*/
		} catch (OwnException e) {
			viewable.setData(DataAccessible.ERROR, e.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

}
