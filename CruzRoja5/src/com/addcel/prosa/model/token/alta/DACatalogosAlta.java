package com.addcel.prosa.model.token.alta;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.UtilSecurity;
import com.addcel.prosa.dto.ObjetosAlta;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.registro.VRegistro;
import com.addcel.prosa.view.registro.VRegistroUpdate;

public class DACatalogosAlta extends DataAccessible implements Runnable {

	final private String TARJETAS = "tarjetas";
	final private String PROVEEDORES = "proveedores";
	final private String ESTADOS = "estados";

	final static public int REGISTRO = 1;
	final static public int ACTUALIZACION = 2;
	
	private int option = 0;
	
	public DACatalogosAlta(Viewable viewable, String data, int option) {
		super(viewable, data);
		
		this.option = option;
	}

	
	public void run() {

		execute(viewable, data);
	}

	
	protected void execute(Viewable viewable, String json) {
		
		try {
			connectable = new MethodGET(URL.URL_GET_CARDTYPES);
			connectable.execute(null);
			String encryptTarjetas = (String) connectable.getData();
			String jsonTarjetas = UtilSecurity.aesDecrypt(UtilSecurity.key, encryptTarjetas);
			System.out.println(jsonTarjetas);

			ObjetosAlta[] tarjetas = createObjects(jsonTarjetas, TARJETAS, false);
			
			
			connectable.setUrl(URL.URL_GET_ESTADOS);
			connectable.execute(null);
			String encryptEstados = (String) connectable.getData();
			String jsonEstados = UtilSecurity.aesDecrypt(UtilSecurity.key, encryptEstados);
			System.out.println(jsonEstados);
			
			ObjetosAlta[] estados = createObjects(jsonEstados, ESTADOS, false);
			
			
			connectable.setUrl(URL.URL_GET_PROVIDERS);
			connectable.execute(null);
			String encryptProveedores = (String) connectable.getData();
			String jsonProveedores = UtilSecurity.aesDecrypt(UtilSecurity.key, encryptProveedores);
			System.out.println(jsonProveedores);
			
			ObjetosAlta[] proveedores = createObjects(jsonProveedores, PROVEEDORES, true);
			
			synchronized (Application.getEventLock()) {
				
			
				switch (option) {
				case REGISTRO:
					UiApplication.getUiApplication().pushScreen(new VRegistro(estados, tarjetas, proveedores));
					break;
				case ACTUALIZACION:
					UiApplication.getUiApplication().pushScreen(new VRegistroUpdate(estados, tarjetas, proveedores));
					break;
				default:
					break;
				}
			}
			
		} catch (OwnException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	
	public ObjetosAlta[] createObjects(String json, String tipoObjeto, boolean isWS) throws JSONException{
		
		JSONObject jsonObject = new JSONObject(json);
		
		JSONArray jsonArray = jsonObject.getJSONArray(tipoObjeto);
		
		int lengh = jsonArray.length();
		
		ObjetosAlta[] objetosAltas = new ObjetosAlta[lengh];
		
		for(int index = 0; index < lengh; index++){
			
			JSONObject object = jsonArray.getJSONObject(index);
			
			ObjetosAlta objetosAlta = new ObjetosAlta();
			
			objetosAlta.setClave(object.optString("clave"));
			objetosAlta.setDescripcion(object.optString("descripcion"));
			
			if (isWS){
				objetosAlta.setClaveWS(object.optString("claveWS"));
			}
			
			objetosAltas[index] = objetosAlta;
		}
				
		return objetosAltas;
	}
}
