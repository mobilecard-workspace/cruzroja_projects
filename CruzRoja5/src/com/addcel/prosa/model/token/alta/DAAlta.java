package com.addcel.prosa.model.token.alta;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.UtilSecurity;
import com.addcel.apiold.dto.UserBean;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;

public class DAAlta extends DataAccessible implements Runnable {

	public DAAlta(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
		
	}

	protected void execute(Viewable viewable, String json) {

		try {
			connectable = new MethodGET(URL.URL_USER_INSERT);
			connectable.execute(json);
			String encryptAlta = (String) connectable.getData();

			encryptAlta = UtilSecurity.aesDecrypt(UtilSecurity.parsePass(UserBean.passTEMP), encryptAlta);

			JSONObject jsonObject = new JSONObject(encryptAlta);
			
			
			final String mensaje = jsonObject.optString("mensaje");
			
			
			UiApplication.getUiApplication().invokeLater(new Runnable() {
				public void run() {
					Dialog.alert(mensaje);
				}
			});
			
			
			System.out.println(encryptAlta);
			
		} catch (OwnException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
