package com.addcel.prosa.model.token.alta;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.security.AddcelCrypto;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;

public class DATerminos extends DataAccessible implements Runnable {

	public DATerminos(Viewable viewable, String data) {
		super(viewable, data);
	}

	public void run() {
		execute(viewable, data);
		
	}

	protected void execute(Viewable viewable, String json) {

		try {
			connectable = new MethodGET(URL.URL_TERMS);
			connectable.execute(json);
			String encryptTerminos = (String) connectable.getData();
			String jsonTerminos = AddcelCrypto.decryptHard(encryptTerminos);
			JSONObject jsonObject = new JSONObject(jsonTerminos);
			
			JSONArray jArray = jsonObject.getJSONArray("consultaTerminosCondiciones");
			
			if (jArray.length() > 0){
				
				JSONObject object = jArray.getJSONObject(0);

				final String desc_termino = object.optString("desc_termino");
				
				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						Dialog.alert(desc_termino);
					}
				});
			}
			

		} catch (OwnException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
