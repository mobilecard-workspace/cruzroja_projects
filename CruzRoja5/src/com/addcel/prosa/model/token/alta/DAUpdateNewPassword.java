package com.addcel.prosa.model.token.alta;

import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.dataaccess.components.connection.http.MethodGET;
import com.addcel.api.util.UtilSecurity;
import com.addcel.api.util.Utils;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;

public class DAUpdateNewPassword extends DataAccessible implements Runnable {

	private String newPassword;
	
	public DAUpdateNewPassword(Viewable viewable, String data, String newPassword) {
		super(viewable, data);

		this.newPassword = newPassword;
	}

	public void run() {

		execute(viewable, data);
		
	}

	protected void execute(Viewable viewable, String json) {

		try {
			connectable = new MethodGET(URL.URL_UPDATE_PASS_MAIL);
			connectable.execute(json);
			String encryptResultado = (String) connectable.getData();

			String jsonResultado = UtilSecurity.aesDecrypt(Utils.parsePass(newPassword), encryptResultado);
			
			JSONObject jsonObject = new JSONObject(jsonResultado);
			
			
			final String mensaje = jsonObject.optString("mensaje");
			
			
			UiApplication.getUiApplication().invokeLater(new Runnable() {
				public void run() {
					Dialog.alert(mensaje);
				}
			});
			
			//System.out.println(jsonResultado);
			
		} catch (OwnException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
