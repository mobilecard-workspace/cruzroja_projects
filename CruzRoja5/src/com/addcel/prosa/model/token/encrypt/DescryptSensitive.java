package com.addcel.prosa.model.token.encrypt;

import com.addcel.api.dataaccess.components.descryption.Descryptionable;
import com.addcel.api.util.security.AddcelCrypto;

public class DescryptSensitive implements Descryptionable{

	private String data;
	
	public void execute(String data) {

		this.data = AddcelCrypto.decryptSensitive(data);
	}

	public Object getData() {

		return data;
	}

}
