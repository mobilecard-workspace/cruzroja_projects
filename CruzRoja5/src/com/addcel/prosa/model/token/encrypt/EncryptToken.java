package com.addcel.prosa.model.token.encrypt;

import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.util.security.AddcelCrypto;

// Encryptor

public class EncryptToken implements Encryptionable {

	String data = null;
	
	public void execute(String data) {

		this.data = AddcelCrypto.encryptHard(data);
	}

	public Object getData() {

		return data;
	}

}
