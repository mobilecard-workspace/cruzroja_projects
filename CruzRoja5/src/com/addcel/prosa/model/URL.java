package com.addcel.prosa.model;

public class URL {

	//public static String URL = "http://50.57.192.214:8080";
	public static String URL = "http://www.mobilecard.mx:8080";
	
	public static String URL_GET_TOKEN = URL + "/CruzRojaServicios/getToken";
	public static String URL_GET_CATALOGO_MONTOS = URL + "/CruzRojaServicios/getMontos";
	public static String URL_GET_PAGOS = URL + "/CruzRojaServicios/pago-visa_";
	public static String URL_GET_PAGOS_ADDCEL = URL + "/CruzRojaServicios/pago-visa";
	public static String URL_LOGIN = URL + "/AddCelBridge/Servicios/adc_userLogin.jsp";
	public static String URL_UPDATE_PASS_MAIL = URL + "/AddCelBridge/Servicios/adc_userPasswordUpdateMail.jsp";
	
	public static String URL_ESTADOS = URL + "/CruzRojaServicios/catalogoEstados";
	public static String URL_DESASTRES = URL + "/CruzRojaServicios/consultaDesastres";
	public static String URL_MONTOS = URL + "/CruzRojaServicios/getMontos";
	
	public static String URL_VERSIONADOR = "http://www.mobilecard.mx:8080/Vitamedica/getVersion?idApp=7&idPlataforma=3";
	
	public static String URL_GET_BANKS = URL + "/AddCelBridge/Servicios/adc_getBanks.jsp";
	public static String URL_GET_CARDTYPES = URL + "/AddCelBridge/Servicios/adc_getCardType.jsp";
	public static String URL_GET_PROVIDERS = URL + "/AddCelBridge/Servicios/adc_getProviders.jsp";
	public static String URL_GET_ESTADOS = URL + "/AddCelBridge/Servicios/adc_getEstados.jsp";
	
	public static String URL_USER_INSERT = URL + "/AddCelBridge/Servicios/adc_userInsert.jsp";
	
	public static String URL_TERMS = URL + "/GDFServicios/ConsumidorTerminos";
	
	public static String ADC_USER_UPDATE = URL + "/AddCelBridge/Servicios/adc_userUpdate.jsp";
	
	public static String URL_GET_USERDATA = URL + "/AddCelBridge/Servicios/adc_getUserData.jsp";
	
	
}
