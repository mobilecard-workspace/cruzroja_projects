package com.addcel.prosa.model.versionador;

import net.rim.blackberry.api.browser.Browser;
import net.rim.blackberry.api.browser.BrowserSession;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;

import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;

public class VVersionador extends Viewable {

	public FontFamily alphaSansFamily = null;
	
	public VVersionador(String title, final String url) {
		
		super(true, title);
		
		try {
			alphaSansFamily = FontFamily.forName("BBAlpha Serif");
			LabelField importante = new LabelField("�IMPORTANTE!");
			
			add(new NullField());
			
			importante.setFont(alphaSansFamily.getFont(Font.PLAIN, 10, Ui.UNITS_pt));
			add(importante);
			
			LabelField nueva = new LabelField("Hay una nueva versi�n de Tesoreria GDF disponible.");
			nueva.setFont(alphaSansFamily.getFont(Font.PLAIN, 7, Ui.UNITS_pt));
			add(nueva);
			
			CustomSelectedSizeButton descarga = new CustomSelectedSizeButton("Descargar", 1){
				
				protected boolean navigationClick(int status, int time) {
					BrowserSession browserSession; 
	    			browserSession = Browser.getDefaultSession();

	    			browserSession.displayPage(url);
					return true;
				}
			};
			
			add(descarga);
			
			LabelField aclaracion = new LabelField(
					"Dudas o aclaraciones cont�ctanos a soporte@addcel.com " +
					"o al t�lefono en el D.F. y �rea metropolitana 5257 5140 " +
					"o del interior de la rep�blica al 01 800 122 33 24");
			
			aclaracion.setFont(alphaSansFamily.getFont(Font.PLAIN, 5, Ui.UNITS_pt));
			
			add(aclaracion);
			
		} catch (ClassNotFoundException e) {
			Dialog.alert("El sistema no pudo configurarse para ejecutarse.");
			e.printStackTrace();
		}
	}

	protected void analyzeData(int request, Object object) {
		// TODO Auto-generated method stub
		
	}
}