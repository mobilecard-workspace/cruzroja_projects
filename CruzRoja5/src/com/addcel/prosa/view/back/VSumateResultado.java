package com.addcel.prosa.view.back;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;

import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.view.base.UtilNumber;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomImageButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VSumateResultado extends Viewable implements FieldChangeListener{

	private ElementLabelField conceptoTxt;
	private ElementLabelField montoTxt;
	private ElementLabelField tdcTxt;	
	private ElementLabelField referenciaTxt;
	private ElementLabelField autorizacionTxt;
	
	private ElementLabelField conceptoEdit;
	private ElementLabelField montoEdit;
	private ElementLabelField tdcEdit;
	private ElementLabelField referenciaEdit;
	private ElementLabelField autorizacionEdit;
	
	private CustomImageButton salir;
	
	PagoRespuesta pagoRespuesta;
	
	public VSumateResultado(boolean isSetTitle, String title,
			PagoRespuesta pagoRespuesta) {
		
		super(false, true, "headers_resumendonativo_");

		this.pagoRespuesta = pagoRespuesta;
		
		conceptoTxt = new ElementLabelField("Concepto");
		montoTxt = new ElementLabelField("Monto");
		tdcTxt = new ElementLabelField("TDC");
		referenciaTxt = new ElementLabelField("Referencia");
		autorizacionTxt = new ElementLabelField("Autorización");
		
		conceptoEdit = new ElementLabelField(pagoRespuesta.getTipoDonacion());
		montoEdit = new ElementLabelField(String.valueOf(UtilNumber.formatCurrency(pagoRespuesta.getMonto())));
		tdcEdit = new ElementLabelField(String.valueOf(pagoRespuesta.getTdcM()));
		referenciaEdit = new ElementLabelField(pagoRespuesta.getAddcelR());
		autorizacionEdit = new ElementLabelField(pagoRespuesta.getAutorizacion());
		
		add(conceptoTxt);
		add(conceptoEdit);

		add(montoTxt);
		add(montoEdit);
		add(tdcTxt);
		add(tdcEdit);
		add(referenciaTxt);
		add(referenciaEdit);
		
		if (pagoRespuesta.getResultado() == 1){
			add(autorizacionTxt);
			add(autorizacionEdit);
		}
		
		salir = new CustomImageButton("btn_finalizaro_", "btn_finalizar_");
		salir.setChangeListener(this);
		add(salir);
	}

	public void fieldChanged(Field field, int context) {
		
		if (field == salir){
			System.exit(0);
		}
	}

	protected void analyzeData(int request, Object object) {
	}
}

/*
if (pagoRespuesta.getIdError() == 0){
	System.exit(0);
} else {
	UiApplication.getUiApplication().popScreen(this);
}
*/

