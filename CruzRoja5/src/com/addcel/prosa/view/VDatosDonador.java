package com.addcel.prosa.view;

import java.util.Vector;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.RadioButtonGroup;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.api.util.UtilBB;
import com.addcel.prosa.Start;
import com.addcel.prosa.dto.Desastre;
import com.addcel.prosa.dto.Estado;
import com.addcel.prosa.dto.Monto;
import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.model.token.DAPagoTarjeta;
import com.addcel.prosa.view.back.VSumateResultado;
import com.addcel.prosa.view.base.UtilDate;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomImageButton;
import com.addcel.prosa.view.base.uicomponents.CustomObjectChoiceField;
import com.addcel.prosa.view.base.uicomponents.CustomRadioButtonField;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VDatosDonador extends Viewable implements FieldChangeListener{

	private ElementLabelField emailTxt;
	private ElementLabelField nombreTxt;
	
	private ElementLabelField montoTxt;
	
	private ElementLabelField numeroTarjetaTxt;
	private ElementLabelField cvv2Txt;
	private ElementLabelField vigenciaTxt;
	private ElementLabelField tipoTarjetaTxt;	

	private CustomEditField emailEdit;
	private CustomEditField nombreEdit;
	
	//private CustomObjectChoiceField montoChoice;
	
	private CustomEditField montoEdit;
	private CustomEditField numeroTarjetaEdit;
	private CustomEditField cvv2Edit;

	private CustomObjectChoiceField anio;
	private CustomObjectChoiceField mes;

	private CustomRadioButtonField rbVisa = null;
	private CustomRadioButtonField rbMaster = null;
	
	private CustomImageButton donar;
	
	private int tarjetaCredito = 0;
	
	private Object destinoDonacion;
	private Monto monto;
	
	public VDatosDonador(Object destinoDonacion, Monto monto) {
		
		super(false, true, "header_rdonativo_");

		this.destinoDonacion = destinoDonacion;
		this.monto = monto;
		
		emailTxt = new ElementLabelField("Email:");
		nombreTxt = new ElementLabelField("Nombre:");

		montoTxt = new ElementLabelField("Donaci�n:");

		numeroTarjetaTxt = new ElementLabelField("N�mero de tarjeta");
		cvv2Txt = new ElementLabelField("Cvv2");
		vigenciaTxt = new ElementLabelField("Vigencia");
		tipoTarjetaTxt = new ElementLabelField("Tipo de tarjeta");
				
		emailEdit = new CustomEditField(100, EditField.FILTER_EMAIL);
		nombreEdit = new CustomEditField(100, EditField.FILTER_DEFAULT);

		montoEdit = new CustomEditField(String.valueOf(monto.getMonto()), 100, EditField.FILTER_NUMERIC);
		numeroTarjetaEdit = new CustomEditField(16, EditField.FILTER_INTEGER);
		cvv2Edit = new CustomEditField(3, EditField.FILTER_INTEGER);
		
		anio = new CustomObjectChoiceField("A�o", UtilDate.getYears(7), 2);
		mes = new CustomObjectChoiceField("Mes", UtilDate.getNumberMonths(), 5);
		
        RadioButtonGroup rbg = new RadioButtonGroup();

        rbVisa = new CustomRadioButtonField("Visa",rbg,true);
        rbMaster = new CustomRadioButtonField("Mastercard",rbg,false);
        
		donar = new CustomImageButton("btn_donaro_", "btn_donar_");
		donar.setChangeListener(this);

		add(emailTxt);
		add(emailEdit);
		add(new LabelField());
		add(nombreTxt);
		add(nombreEdit);
		//add(new LabelField());
		//add(montoChoice);
		add(new LabelField());
		add(montoTxt);
		add(montoEdit);
		add(new LabelField());
		add(numeroTarjetaTxt);
		add(numeroTarjetaEdit);
		add(new LabelField());
		add(cvv2Txt);
		add(cvv2Edit);
		add(new LabelField());
		add(vigenciaTxt);
		add(mes);
		add(anio);
		add(new LabelField());
		add(tipoTarjetaTxt);
		add(rbVisa);
        add(rbMaster);
		add(new LabelField());
		add(donar);

	}

	
	public void fieldChanged(Field arg0, int arg1) {

			if(arg0 == donar){
			
			if (rbVisa.isSelected()){
				tarjetaCredito = 1;
			} else if (rbMaster.isSelected()){
				tarjetaCredito = 2;
			} else {
				tarjetaCredito = 0;
			}
			
			
			if (tarjetaCredito > 0){
				
				
				if (checarCampos()){

					String sAnio = getInfo(anio).substring(2);
					String vigencia = getInfo(mes) + "/" + sAnio;
					
					JSONObject jsonObject = new JSONObject();

					try {

						String producto = String.valueOf(monto.getIdMonto());
						
						if (destinoDonacion instanceof Desastre){
							
							Desastre desastre = (Desastre)destinoDonacion;

							jsonObject.put("idDesastre", desastre.getIdDesastre());
						} else if (destinoDonacion instanceof Estado){
							
							Estado estado = (Estado)destinoDonacion;

							jsonObject.put("idEstado", estado.getIdEstado());
						}

						jsonObject.put("email", emailEdit.getText());
						jsonObject.put("nombre", nombreEdit.getText());
						jsonObject.put("tarjeta", numeroTarjetaEdit.getText());
						jsonObject.put("cvv2", cvv2Edit.getText());
						jsonObject.put("vigencia", vigencia);
						jsonObject.put("producto", producto);
						jsonObject.put("monto", String.valueOf(monto.getMonto()));
						
						jsonObject.put("imei", UtilBB.getImei());
						jsonObject.put("cx", "0.0");
						jsonObject.put("cy", "0.0");
						jsonObject.put("tipo", UtilBB.getDeviceINFO(UtilBB.MANUFACTURER_NAME));
						jsonObject.put("software", UtilBB.getDeviceINFO(UtilBB.SOFTWARE_VERSION));
						jsonObject.put("modelo", UtilBB.getDeviceINFO(UtilBB.DEVICE_NAME));
						jsonObject.put("key", UtilBB.getImei());

						//http://50.57.192.213:8080/CruzRojaServicios/pago-visa_

						
						JSONObject jsonObject1 = new JSONObject();
						jsonObject1.put("idProveedor", Start.ID_PROVEEDOR);
						jsonObject1.put("usuario", "userPrueba");
						jsonObject1.put("password", "passwordPrueba");
						
						DAPagoTarjeta pagoTarjeta = new DAPagoTarjeta(this, jsonObject1.toString(), String.valueOf(monto.getMonto()));
						pagoTarjeta.setJson(jsonObject);
						Thread thread = new Thread(pagoTarjeta);
						thread.start();
						
						
					} catch (JSONException e) {
						this.analyzeData(DataAccessible.ERROR, Error.JSON_EXCEPTION);
						e.printStackTrace();
					}
					

				} else {
					this.analyzeData(DataAccessible.ERROR, "Existen campos sin informaci�n.");
				}
			} else {
				
				this.analyzeData(DataAccessible.ERROR, "No ha seleccionado un tipo de tarjeta.");
			}
		}
	}

	protected void analyzeData(int request, Object object) {
		
		
		if (request == DataAccessible.ERROR){
			
			Dialog.alert((String)object);
		}
		
		
		
		else if (request == DataAccessible.DATA){
			
			if (object instanceof Vector){
				
				Vector montos = (Vector)object;
				int size = montos.size();
				Monto catmontos[] = new Monto[size];
				montos.copyInto(catmontos);
				//montoChoice.setChoices(catmontos);
			} else if(object instanceof PagoRespuesta){
				
				UiApplication.getUiApplication().pushScreen(new VSumateResultado(true, "", (PagoRespuesta)object));
			}
			
			
		}
		
		
		
	}
	
	
	
	private boolean checarCampos(){
		
		boolean check = true;
		
		if (invalidText(emailEdit)){
			check = false;
		} else if (invalidText(nombreEdit)){
			check = false;
		} else if (invalidText(numeroTarjetaEdit)){
			check = false;
		} else if (invalidText(cvv2Edit)){
			check = false;
		}
		
		return check;
	}
	

	private boolean invalidText(CustomEditField editField){
		
		String temp = editField.getText();
		
		if ((temp != null)&&(temp.length()>0)){
			return false;
		} else {
			return true;
		}
	}
	
	
	
	private String getInfo(ObjectChoiceField choiceField){
		
		String temp = null;
		
		int index = choiceField.getSelectedIndex();
		
		Object object = choiceField.getChoice(index);
		
		if (object instanceof String){
			
			temp = (String)object;
		} else {
			
			Monto monto = (Monto)object;
			temp = String.valueOf(monto.getMonto());
		}
		
		return temp;
	}
}
