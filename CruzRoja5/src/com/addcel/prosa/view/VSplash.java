package com.addcel.prosa.view;

import java.util.Timer;
import java.util.TimerTask;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

import com.addcel.prosa.model.token.DAVersionador;
import com.addcel.prosa.view.base.UtilIcon;
import com.addcel.prosa.view.base.Viewable;

public class VSplash extends Viewable {

	public VSplash() {

		super(false, null);

		BitmapField splash = UtilIcon.getSplash();

		add(splash);
		
		Timer timer = new Timer();

		Wait wait = new Wait(this);
		
		timer.schedule(wait, 4000);
	}

	class Wait extends TimerTask {

		private VSplash viewSplash;

		public Wait(VSplash viewSplash) {

			this.viewSplash = viewSplash;
			
			//UiApplication.getUiApplication().pushScreen(new VConfiguracionDatos());
			
			DAVersionador daVersionador = new DAVersionador(new VConfiguracionDatos(), null);
			Thread thread = new Thread(daVersionador);
			thread.start();
			
		}

		public void run() {

			synchronized (Application.getEventLock()) {
				//UiApplication.getUiApplication().popScreen(viewSplash);
				//UiApplication.getUiApplication().pushScreen(new VInicio());
			}
		}
	}
	
	protected void analyzeData(int request, Object object) {
	}
}



/*
class Wait extends TimerTask {

	private VSplash viewSplash;

	public Wait(VSplash viewSplash) {

		this.viewSplash = viewSplash;
	}

	public void run() {

		UiApplication.getUiApplication().invokeAndWait(new Runnable() {

			public void run() {
				UiApplication.getUiApplication().popScreen(viewSplash);
				//UiApplication.getUiApplication().pushScreen(new VLogin(true, "Acceso"));
				UiApplication.getUiApplication().pushScreen(new VConfiguracionDatos());
				
			}
		});
	}
}
*/