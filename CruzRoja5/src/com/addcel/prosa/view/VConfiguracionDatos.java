package com.addcel.prosa.view;

import java.util.Hashtable;
import java.util.Vector;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.api.dataaccess.DataAccessible;
import com.addcel.apiold.dto.UserBean;
import com.addcel.prosa.dto.Desastre;
import com.addcel.prosa.dto.Estado;
import com.addcel.prosa.dto.Monto;
import com.addcel.prosa.model.passupdate.PasswordUpdater;
import com.addcel.prosa.model.token.DACatalogos;
import com.addcel.prosa.model.token.alta.DACatalogosAlta;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomImageButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VConfiguracionDatos extends Viewable implements FieldChangeListener{

	private LabelField labelEstados;
	private LabelField labelDesastres;
	private LabelField labelMontos;
	private LabelField labelMonto;
	
	private ObjectChoiceField choiceEstados;
	private ObjectChoiceField choiceDesastres;
	private ObjectChoiceField choiceMontos;
	
	private CheckboxField checkboxEstado;
	private CheckboxField checkboxDesastre;
	
	private BasicEditField editMonto;
	
	private CustomImageButton aceptar;
	
	private String montoMaximo;
	
	private VConfiguracionDatos configuracionDatos;
	
	public VConfiguracionDatos() {
		
		super(false, true, "header_apoya_");

		this.configuracionDatos = this;
		
		DACatalogos daEstados = new DACatalogos(this, null);
		Thread thread = new Thread(daEstados);
		thread.start();
		
		String mensaje =
		"Si deseas simpliflicar tu donativo, puedes iniciar sesi�n con tu usuario MOBILECARD. " +
		"Si no eres usuario, selecciona REGISTRO dentro del men�";
		
		RichTextField labelMensaje = new RichTextField(mensaje, RichTextField.TEXT_ALIGN_HCENTER|RichTextField.NON_FOCUSABLE){
			
			protected void paint(Graphics graphics) {
			    graphics.setColor(Color.RED);
			    super.paint(graphics);
			}
		};

		labelEstados = new ElementLabelField("Elige estado:");
		labelDesastres = new ElementLabelField("Elige desastre:");
		labelMontos = new ElementLabelField("Cantidad a donar:");
		labelMonto = new ElementLabelField("Otra cantidad a donar:");
		
		choiceEstados = new ObjectChoiceField();
		choiceDesastres = new ObjectChoiceField();
		choiceMontos = new ObjectChoiceField();
		
		editMonto = new BasicEditField(BasicEditField.FILTER_NUMERIC);
		Background background = BackgroundFactory.createSolidBackground(Color.LIGHTGREY);
		editMonto.setBackground(background);
		
		
        checkboxEstado = new CheckboxField("Estado", true){
			
			protected void paint(Graphics graphics) {
			    graphics.setColor(Color.RED);
			    super.paint(graphics);
			}
		};
        checkboxDesastre = new CheckboxField("Desastre", false){
			
			protected void paint(Graphics graphics) {
			    graphics.setColor(Color.RED);
			    super.paint(graphics);
			}
		};
        checkboxEstado.setChangeListener(this);
        checkboxDesastre.setChangeListener(this);
		
		aceptar = new CustomImageButton("btn_continuaro_", "btn_continuar_");
		aceptar.setChangeListener(this);
		
		add(labelEstados);
		add(choiceEstados);
		add(labelDesastres);
		add(choiceDesastres);
        add(checkboxEstado);
        add(checkboxDesastre);
        add(new LabelField());
		add(labelMontos);
		add(choiceMontos);
		add(labelMonto);
		add(editMonto);
		add(aceptar);
		add(labelMensaje);
		
		addMenuItem(mIniciarSesion);
		addMenuItem(mRegistro);
		//addMenuItem(mEditarRegistro);
	}


	public void fieldChanged(Field field, int context) {

		if (field == aceptar){
			
			Object object = null;
			
			if (checkboxEstado.getChecked()){
				
				object = getDataFromChoice(choiceEstados);
			} else if (checkboxDesastre.getChecked()){
				object = getDataFromChoice(choiceDesastres);
			}
			
			String sMonto = editMonto.getText();
			
			Monto monto = null;
			
			if ((sMonto != null)&&(sMonto.length() > 0)){
				
				double dMonto = Double.parseDouble(sMonto);

				monto = new Monto();
				
				monto.setIdMonto("9000");
				monto.setMonto(sMonto);
				
			} else {
				
				monto = (Monto)getDataFromChoice(choiceMontos);
			}
			
			System.out.println(monto);

			double dMontoMaximo = Double.parseDouble(montoMaximo);
			
			if (dMontoMaximo >= Double.parseDouble(monto.getMonto())){
				
				if (UserBean.idLogin == null){
				
					UiApplication.getUiApplication().pushScreen(new VDatosDonador(object, monto));
				} else {
					
					UiApplication.getUiApplication().pushScreen(new VDatosDonadorMB(object, monto));
				}
				
			} else {
				
				Dialog.alert("Se le agradecen sus buenos deseos, pero la donaci�n no puede ser mas de " + montoMaximo);
				
			}
			
			
		} else if (field == checkboxEstado){
			
			checkboxDesastre.setChangeListener(null);
			checkboxDesastre.setChecked(false);
			checkboxDesastre.setChangeListener(this);
		} else if (field == checkboxDesastre){
			
			checkboxEstado.setChangeListener(null);
			checkboxEstado.setChecked(false);
			checkboxEstado.setChangeListener(this);
		}
		
	}

	private Object getDataFromChoice(ObjectChoiceField choice){
		
		int index = choice.getSelectedIndex();
		Object object = choice.getChoice(index);
		return object;
	}
	
	protected void analyzeData(int request, Object object) {

		if (request == DataAccessible.DATA){
			
			Hashtable elements = (Hashtable)object;
			
			Vector estados = (Vector)elements.get("estados");
			Vector desastres = (Vector)elements.get("desastres");		
			Vector montos = (Vector)elements.get("montos");
			montoMaximo = (String)elements.get("montoMaximo");
			
			int size = estados.size();
			Estado[] cadenaEstados = new Estado[size];
			estados.copyInto(cadenaEstados);
			
			size = desastres.size();
			Desastre[] cadenaDesastres = new Desastre[size];
			desastres.copyInto(cadenaDesastres);
			
			size = montos.size();
			Monto[] cadenaMonto = new Monto[size];
			montos.copyInto(cadenaMonto);
			
			choiceEstados.setChoices(cadenaEstados);
			choiceDesastres.setChoices(cadenaDesastres);
			choiceMontos.setChoices(cadenaMonto);
			
		} else if(request == DataAccessible.ERROR){
			
			String error = (String)object;
			Dialog.alert(error);
		}
	}
	
	private MenuItem mIniciarSesion = new MenuItem("Iniciar sesi�n", 110, 10) {
		public void run() {
			UiApplication.getUiApplication().pushScreen(new VLogin(configuracionDatos));
		}
	};
	
	private MenuItem mRegistro = new MenuItem("Registro", 110, 10) {
		public void run() {
			//UiApplication.getUiApplication().pushScreen(new VRegistro(configuracionDatos));
			//CreditInfoThread creditInfoThread = new CreditInfoThread("proveedores", CreditInfoThread.REGISTRO);
			//creditInfoThread.run();
			
			DACatalogosAlta daCatalogosAlta = new DACatalogosAlta((Viewable)configuracionDatos, null, DACatalogosAlta.REGISTRO);
			Thread thread = new Thread(daCatalogosAlta);
			thread.start();
		}
	};
	
	private MenuItem mEditarRegistro = new MenuItem("Actualizar Registro", 110, 10) {
		public void run() {
			
			UiApplication.getUiApplication().pushScreen(new PasswordUpdater(null));
			
			System.out.println("mEditarRegistro");
		}
	};
}


/*



   private class CheckboxListener implements FieldChangeListener {

      public void fieldChanged(Field field, int context) {

         if (context != FieldChangeListener.PROGRAMMATIC) {
            // user modified this field
            CheckboxField checkbox = (CheckboxField)field;   
            if (checkbox.getChecked()) {              
               // uncheck the other checkboxes
               for (int i = 0; i < checkBoxField.length; i++) {
                  if (checkBoxField[i] != checkbox && checkBoxField[i].getChecked()) {
                     checkBoxField[i].setChecked(false);
                  }
               }
            }
         } else {  
            // nothing more to do here ... this time, fieldChanged() is being
            //  called as a result of calling setChecked() in the code.
         }
      }      
   }




*/



/*
String[] estados = 
	{"Distrito Federal", "Aguascalientes", "Baja California", 
	"Baja California Sur", "Campeche", "Chiapas", 
	"Chihuahua", "Coahuila", "Colima", "Durango", 
	"Guanajuato", "Guerrero", "Hidalgo", "Jalisco", 
	"Estado de M�xico", "Michoac�n", "Morelos", 
	"Nayarit", "Nuevo Le�n", "Oaxaca", "Puebla", 
	"Quer�taro", "Quintana Roo", "San Luis Potos�", 
	"Sinaloa", "Sonora", "Tabasco", "Tamaulipas", 
	"Tlaxcala", "Veracruz", "Yucat�n", "Zacatecas"};

String[] desastres = {"inundacion", "terremoto", "lluvia"};

String[] montos = {"100.00", "200.00", "300.00", "400.00", "500.00", "600.00"};
*/

/*
choiceEstados = new ObjectChoiceField("", estados);
choiceDesastres = new ObjectChoiceField("", desastres);
choiceMontos = new ObjectChoiceField("", montos);
*/

/*
labelEstados = new LabelField("Selecciona un estado:");
labelDesastres = new LabelField("Selecciona un desastre:");
labelMontos = new LabelField("Selecciona un monto:");
labelMonto = new LabelField("Escribe un monto:");
*/
