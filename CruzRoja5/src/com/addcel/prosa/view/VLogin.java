package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.apiold.Login;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomEditField;
import com.addcel.prosa.view.base.uicomponents.CustomImageButton;
import com.addcel.prosa.view.base.uicomponents.CustomPasswordEditField;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VLogin extends Viewable implements FieldChangeListener{

	private ElementLabelField userTxt;
	private ElementLabelField passwordTxt;
	
	private CustomEditField userEdit;
	private CustomPasswordEditField passwordEdit;
	
	private CustomImageButton iniciarSesion;
	private VConfiguracionDatos configuracionDatos;
	
	public VLogin(VConfiguracionDatos configuracionDatos) {
		
		super(false, true, "header_iniciosesion_");

		this.configuracionDatos = configuracionDatos;
		
		userTxt = new ElementLabelField("Usuario");
		passwordTxt = new ElementLabelField("Contraseņa");
		
		userEdit = new CustomEditField(16, EditField.CONSUME_INPUT);
		passwordEdit = new CustomPasswordEditField(12, EditField.CONSUME_INPUT);
		
		iniciarSesion = new CustomImageButton("btn_iniciosesiono_", "btn_iniciosesion_");
		iniciarSesion.setChangeListener(this);
		
		add(userTxt);
		add(userEdit);
		
		add(passwordTxt);
		add(passwordEdit);
		add(new LabelField());
		add(iniciarSesion);
		//add(donar);
	}

	public void fieldChanged(Field field, int context) {
		
		
		if (field == iniciarSesion){
			
			//VConfiguracionDatos configuracionDatos = new VConfiguracionDatos();
			System.out.println("configuracionDatos");
			Login login = new Login(configuracionDatos, this, userEdit.getText(), passwordEdit.getText());
			login.run();
			
		} 
		
		/*
		else if(field == donar){
			UiApplication.getUiApplication().pushScreen(new VSumateDatos(true, "Datos para donar"));
		}
		*/
	}

	protected void analyzeData(int request, Object object) {
		
		System.out.println(object);
		
	}

}
