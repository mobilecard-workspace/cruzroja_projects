package com.addcel.prosa.view.base.uicomponents;


import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class ElementLabelField extends LabelField{

	private String text;
	int width = 0;
	
	public ElementLabelField(String text){
		super(text, LabelField.NON_FOCUSABLE);
		configuracion();
	}
	
	
	public ElementLabelField(String text, long style ){
		super(text, style);
		configuracion();
	}
	
	private void configuracion(){
		this.text = text;
		width = Display.getWidth();
		Background background = BackgroundFactory.createSolidBackground(UtilColor.ELEMENT_BACKGROUND);
		setBackground(background);
	}
	
	public void paint(Graphics graphics) {

		graphics.setColor(UtilColor.ELEMENT_STRING);
		graphics.drawText(text, 0, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}
