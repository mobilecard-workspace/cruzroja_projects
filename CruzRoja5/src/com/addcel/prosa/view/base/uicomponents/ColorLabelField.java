package com.addcel.prosa.view.base.uicomponents;

import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.LabelField;

public class ColorLabelField extends LabelField{

	private String text;
	int width = 0;
	int color;
	
	public ColorLabelField(String text){
		super(text, LabelField.NON_FOCUSABLE);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
	}

	
	public ColorLabelField(String text, long style ){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
		color = UtilColor.LABEL_FIELD_STRING;
	}
	
	
	public ColorLabelField(String text, long style, int color){
		super(text, style);
		this.text = text;
		width = Display.getWidth();
		this.color = color;
	}

	public void paint(Graphics graphics) {

		graphics.setColor(color);
		graphics.drawText(text, 0, 0, DrawStyle.LEFT, width);

		super.paint(graphics);
	}
}