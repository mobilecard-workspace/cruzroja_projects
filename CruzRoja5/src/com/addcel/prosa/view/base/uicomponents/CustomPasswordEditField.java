package com.addcel.prosa.view.base.uicomponents;

import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;


public class CustomPasswordEditField extends PasswordEditField{

	
	private Background focus;
	private Background unFocus;
	
	public CustomPasswordEditField(int maxNumChars, long style){
		
		super("", "", maxNumChars, style);
		setUI();
	}
	
	
	
	public CustomPasswordEditField(String value){

		super("", value); 
		setUI();
	}
	
	public CustomPasswordEditField(){

		super(); 
		setUI();
	}
	
	private void setUI(){
		focus = BackgroundFactory.createSolidBackground(UtilColor.EDIT_TEXT_BACKGROUND_FOCUS);
		unFocus = BackgroundFactory.createSolidBackground(UtilColor.EDIT_TEXT_BACKGROUND_UNFOCUS);
		setBackground(unFocus);
	}
	
	protected void paint(Graphics graphics) {

		if (isFocus()){
			graphics.setColor(UtilColor.EDIT_TEXT_DATA_FOCUS);
		} else {
			graphics.setColor(UtilColor.EDIT_TEXT_DATA_UNFOCUS);
		}
		
		super.paint(graphics);
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		setBackground(focus);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		setBackground(unFocus);
		invalidate();
	}
}
