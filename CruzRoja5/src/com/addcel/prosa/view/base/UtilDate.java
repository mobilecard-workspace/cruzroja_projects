package com.addcel.prosa.view.base;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class UtilDate {

	
	String meses[] = {"enero", "febrero", "marzo", 
			  "abril", "mayo", "junio", "julio", 
			  "agosto", "septiembre", "octubre", 
			  "noviembre", "diciembre"};
	
	
	public static String[] getMonths(){
		String meses[] = {"enero", "febrero", "marzo", 
				  "abril", "mayo", "junio", "julio", 
				  "agosto", "septiembre", "octubre", 
				  "noviembre", "diciembre"};
		return meses;
	}
	
	
	
	public static String[] getNumberMonths(){
		String meses[] = {"01", "02", "03", 
				  "04", "05", "06", "07", 
				  "09", "09", "10", 
				  "11", "12"};
		return meses;
	}
	
	
	public static String[] getYears(int value){
		
		String[] years = null;
		Vector vector = new Vector();
		
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());
		
		int year = calendar.get(calendar.YEAR);
		
		if (value < 0){
			for (int index = year; index > (year + value); index--){
				
				vector.addElement(String.valueOf(index));
			}
		} else {
			for (int index = year; index < (year + value); index++){
				
				vector.addElement(String.valueOf(index));
			}
		}
		
		years = new String[vector.size()];
		
		vector.copyInto(years);
		
		return years;
	}
	
	
	
	public static String getDateToDDMMAAAA(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int iYear = calendar.get(Calendar.YEAR);
		int iMonth = calendar.get(Calendar.MONTH);
		int iDay = calendar.get(Calendar.DAY_OF_MONTH);

		String sYear = Integer.toString(iYear);
		String sMonth = Integer.toString(iMonth + 1);
		String sDay = Integer.toString(iDay);

		sMonth = (sMonth.length() == 1) ? "0" + sMonth : sMonth;
		sDay = (sDay.length() == 1) ? "0" + sDay : sDay;
		
		return sDay + sMonth + sYear;
	}
	
	
	
	public static String getYYYY_MM_DDFromLong(long longDate){
		
		/*
			YYYY-MM-DD from long
		*/
		
		StringBuffer value = new StringBuffer();
		String dash = "-";
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(new Date(longDate));
		
		int year = calendar.get(calendar.YEAR);
		int month = calendar.get(calendar.MONTH) + 1;
		int day = calendar.get(calendar.DATE);
		
		String strYear = String.valueOf(year);
		String strMonth = String.valueOf(month);
		String strDay = String.valueOf(day);

		if (month < 10) {
			strMonth = "0" + strMonth;
		}

		if (day < 10) {
			strDay = "0" + strDay;
		}

		
		value.append(strYear).append(dash).append(strMonth).append(dash).append(strDay); 
		
		
		return value.toString();
	}
}
