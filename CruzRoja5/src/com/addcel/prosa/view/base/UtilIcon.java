package com.addcel.prosa.view.base;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.BitmapField;

public class UtilIcon {

	
	public static BitmapField getTitle(){
		
		String resolution = "w" + String.valueOf(Display.getWidth())  + ".png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}

	
	
	public static BitmapField getTitleBoleto(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boletoa.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
	
	public static BitmapField getLogo(String subheader){
		
		String nameBmp = subheader + String.valueOf(Display.getWidth())  + ".png";
		Bitmap encodedImage = Bitmap.getBitmapResource(nameBmp);
		BitmapField bitmapField = new BitmapField(encodedImage, BitmapField.FIELD_HCENTER);

		return bitmapField;
	}
	
	
	public static Bitmap getBitMap(String data){
		
		String imageName = data + String.valueOf(Display.getWidth())  + ".png";
		Bitmap encodedImage = Bitmap.getBitmapResource(imageName);
		return encodedImage;
	}
	
	public static BitmapField getBodyBoleto(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boleto.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
	
	public static Bitmap getBodyBoletoBitmap(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boleto.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		return encodedImage;
	}


	public static BitmapField getSplash(){
		
		int width = Display.getWidth();
		int height = Display.getHeight();
		
		StringBuffer splash = new StringBuffer();
		splash.append("splash");
		splash.append(width).append("x");
		splash.append(height);
		splash.append(".png");
		
		Bitmap encodedImage = Bitmap.getBitmapResource(splash.toString());
		
		if (encodedImage == null){
			
			splash = new StringBuffer();
			splash.append("splash_");
			splash.append(width);
			splash.append(".png");
			encodedImage = Bitmap.getBitmapResource(splash.toString());
		}
		
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
}
