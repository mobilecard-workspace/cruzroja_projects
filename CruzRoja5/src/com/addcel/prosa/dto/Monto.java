package com.addcel.prosa.dto;

public class Monto {

/*
	
{
	"montos": [
		{
			"idMonto": 8,
			"monto": 50
		},
		{
			"idMonto": 12,
			"monto": 1000
		}
	],
	"montoMaximo": "5000"
}
	
*/
	private String idMonto;
	private String monto;
	
	public String getIdMonto() {
		return idMonto;
	}
	public void setIdMonto(String idMonto) {
		this.idMonto = idMonto;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String toString(){
		return monto;
	}
}
