package com.addcel.prosa.dto;

/*

{
	"estados": 
	[
		{
			"idEstado": "-1",
			"descripcion": "Selecciona ..."
		},
		{
			"idEstado": "99",
			"descripcion": "Sede Nacional"
		},
		{
			"idEstado": "1",
			"descripcion": "Aguascalientes"
		}
	]
}

*/

public class Estado {

	private String idEstado;
	private String descripcion;
	
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String toString(){
		return descripcion;
	}
}
