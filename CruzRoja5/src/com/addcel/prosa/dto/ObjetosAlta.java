package com.addcel.prosa.dto;

public class ObjetosAlta {

	private String clave;
	private String claveWS;
	private String descripcion;
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getClaveWS() {
		return claveWS;
	}
	public void setClaveWS(String claveWS) {
		this.claveWS = claveWS;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String toString(){
		return descripcion;
	}
}
