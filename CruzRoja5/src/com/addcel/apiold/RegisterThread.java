package com.addcel.apiold;


import java.util.Vector;

import org.json.me.JSONObject;

import com.addcel.api.util.UtilBB;
import com.addcel.api.util.UtilSecurity;
import com.addcel.apiold.dto.UserBean;
import com.addcel.apiold.view.SplashScreen;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;



public class RegisterThread extends Thread implements HttpListener {

	private String url = URL.URL_USER_INSERT;
	private String post = "";
	private SplashScreen splashScreen = null;
	private Viewable viewable = null;

	public RegisterThread(String json, Viewable viewable) {

		this.post = "json=" + json;
		splashScreen = SplashScreen.getInstance();
		this.viewable = viewable;
	}

	public void run() {
		connect();
	}

	public void connect() {

		//Application.getApplication();
		/*
		synchronized (Application.getEventLock()) {
			splashScreen.start();
		}
*/
		
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				splashScreen.start();
			}
		});
		
		
		
		String idealConnection = UtilBB.checkConnectionType();

		if (idealConnection != null) {

			if (this.url != null) {

				try {

					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpPost(this, this.url, this.post);

				} catch (Exception e) {

				}
			}
		} else {
			// Mensaje de error
		}

	}

	public void handleHttpError(int errorCode, String error) {
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());

			sTemp = UtilSecurity.aesDecrypt(
					UtilSecurity.parsePass(UserBean.passTEMP), sTemp);

			//UiApplication.getUiApplication();
			//synchronized (Application.getEventLock()) {

				

				JSONParser jsParser = new JSONParser();

				if (jsParser.isRegister(sTemp)) {

					
					UiApplication.getUiApplication().invokeLater(new Runnable() {
						public void run() {
							splashScreen.remove();
							UiApplication.getUiApplication().popScreen(UiApplication.getUiApplication().getActiveScreen());
						}
					});

				} else {
					
					final JSONObject jsObject = new JSONObject(sTemp);

					UiApplication.getUiApplication().invokeLater(new Runnable() {
						public void run() {
							splashScreen.remove();
							viewable.setData(0, jsObject);
						}
					});

				}
			//}
		} catch (Exception e) {

			getMessageError();
		}
	}

	public void getMessageError() {

		synchronized (Application.getEventLock()) {
			splashScreen.remove();
			// UiApplication.getUiApplication().pushScreen(new
			// MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde",
			// Field.NON_FOCUSABLE));
		}

	}

}
