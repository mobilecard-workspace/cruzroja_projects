package com.addcel.apiold;

import java.util.Vector;

import com.addcel.api.util.UtilBB;
import com.addcel.api.util.UtilSecurity;
import com.addcel.apiold.dto.GeneralBean;
import com.addcel.apiold.view.SplashScreen;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.registro.VRegistro;
import com.addcel.prosa.view.registro.VRegistroUpdate;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.UiApplication;


public class CreditInfoThread extends Thread implements HttpListener {

	final public static int REGISTRO = 0;
	final public static int ACTUALIZACION = 1;
	
	public String url = "";
	public String id = "";

	private static GeneralBean[] EdoBeans = null;
	private static GeneralBean[] bankBeans = null;
	private static GeneralBean[] creditBeans = null;
	private static GeneralBean[] providersBeans = null;
	
	private SplashScreen splashScreen = null;
	private int option;
	
	public CreditInfoThread(String id, int option) {

		this.id = id;
		
		this.option = option;

		if (this.id.equals("bancos")) {

			this.url = URL.URL_GET_BANKS;

		} else if (this.id.equals("tarjetas")) {

			this.url = URL.URL_GET_CARDTYPES;

		} else if (this.id.equals("proveedores")) {

			this.url = URL.URL_GET_PROVIDERS;

		} else if (this.id.equals("estados")) {

			this.url = URL.URL_GET_ESTADOS;

		}
		
		splashScreen = SplashScreen.getInstance();

	}

	
	public CreditInfoThread(String id) {

		this.id = id;
		
		//this.option = option;

		if (this.id.equals("bancos")) {

			this.url = URL.URL_GET_BANKS;

		} else if (this.id.equals("tarjetas")) {

			this.url = URL.URL_GET_CARDTYPES;

		} else if (this.id.equals("proveedores")) {

			this.url = URL.URL_GET_PROVIDERS;

		} else if (this.id.equals("estados")) {

			this.url = URL.URL_GET_ESTADOS;

		}
		
		splashScreen = SplashScreen.getInstance();

	}
	
	
	public void run() {
		connect();
	}

	public void connect() {

		String idealConnection = UtilBB.checkConnectionType();
		
		if (idealConnection != null) {

			if (this.url != null) {
				try {
					Communicator communicator = Communicator.getInstance();
					communicator.sendHttpRequest(this, this.url);

				} catch (Exception t) {
					t.printStackTrace();
				}
			}
		} else {
			//Utils.checkConnectionType();
			//this.connect();
			
			//Error
		}

	}

	public void handleHttpError(int errorCode, String error) {
		getMessageError();
	}

	public boolean isDestroyed() {
		return false;
	}

	public void receiveEstatus(String msg) {
	}

	public void receiveHeaders(Vector _headers) {
	}

	
	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		StringBuffer sb = new StringBuffer();
		String key = "1234567890ABCDEF0123456789ABCDEF";
		JSONParser jsParser = new JSONParser();
		
		try {

			sb.append(new String(response, 0, response.length, "UTF-8"));
			sTemp = new String(sb.toString());

			System.out.println("STEMP: " + sTemp);
			GeneralBean[] gralBeans = null;
			sTemp = UtilSecurity.aesDecrypt(key, sTemp);
			System.out.println("STEMP: " + sTemp);

			gralBeans = jsParser.getInfoCredits(this.id, sTemp);

			if (gralBeans != null) {

				if (gralBeans.length > 0) {

					if (this.id.equals("proveedores")) {

						providersBeans = gralBeans;
						new CreditInfoThread("bancos", option).start();

					} else if (this.id.equals("bancos")) {

						bankBeans = gralBeans;

						new CreditInfoThread("tarjetas", option).start();

					} else if (this.id.equals("tarjetas")) {

						creditBeans = gralBeans;

						new CreditInfoThread("estados", option).start();

					} else if (this.id.equals("estados")) {

						EdoBeans = gralBeans;

						//UiApplication.getUiApplication();

						synchronized (Application.getEventLock()) {
							
							splashScreen.remove();
							
							switch (option) {
							case REGISTRO:
								//UiApplication.getUiApplication().pushScreen(new VRegistro(EdoBeans, bankBeans, creditBeans, providersBeans));
								break;
							case ACTUALIZACION:
								//UiApplication.getUiApplication().pushScreen(new VRegistroUpdate(EdoBeans, bankBeans, creditBeans, providersBeans));
								break;
							default:
								break;
							}
						}
					}

				} else {

					getMessageError();
				}

			} else {

				getMessageError();
			}

		} catch (Exception e) {

			getMessageError();
		}
	}


	public void getMessageError() {

		synchronized (Application.getEventLock()) {
			// MainClass.splashScreen.remove();
			// UiApplication.getUiApplication().pushScreen(new
			// MessagePopupScreen("Informaci�n no disponible. Favor de intentar m�s tarde",
			// Field.NON_FOCUSABLE));
		}
	}
}
