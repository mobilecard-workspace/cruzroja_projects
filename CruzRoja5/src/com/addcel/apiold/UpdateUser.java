package com.addcel.apiold;



import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.util.UtilSecurity;
import com.addcel.api.util.Utils;
import com.addcel.prosa.model.URL;
import com.addcel.prosa.view.base.Viewable;



public class UpdateUser extends HttpListenerExtend {

	
	private String password;
	
	public UpdateUser(String post, Viewable viewable, String URL) {
		super(post, URL, viewable);
	}

	public UpdateUser(Viewable viewable, String url, String post) {
		super(viewable, url, post);
	}

	
	public UpdateUser(Viewable viewable, String url, String post, String password) {
		super(viewable, url, post);
		this.password = password;
	}
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json = new String(response);
		
		//String password = UserBean.password;
		
		json = UtilSecurity.aesDecrypt(Utils.parsePass(password), json);

		try {
			
			jsObject = new JSONObject(json);
			sendData(jsObject);
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de JSON");
		}
	}
}



