package com.addcel.apiold;

import java.util.Vector;

import com.addcel.api.util.UtilSecurity;
import com.addcel.api.util.Utils;
import com.addcel.prosa.view.base.Viewable;


public class UpdateUserPassThread implements HttpListener{
//public class UpdateUserPassThread {

	private String newPassword;
	
	public UpdateUserPassThread(Viewable viewable, String url, String post, String newPassword){

		//super(viewable, url, post);
		
		this.newPassword = newPassword;
	}


	public void receiveHttpResponse(int appCode, byte[] response) {

		String sTemp = null;
		sTemp = new String(response, 0, response.length);
		sTemp = UtilSecurity.aesDecrypt(Utils.parsePass(newPassword), sTemp);
	}


	public void handleHttpError(int errorCode, String error) {
		// TODO Auto-generated method stub
		
	}


	public void receiveEstatus(String msg) {
		// TODO Auto-generated method stub
		
	}


	public void receiveHeaders(Vector _headers) {
		// TODO Auto-generated method stub
		
	}


	public boolean isDestroyed() {
		// TODO Auto-generated method stub
		return false;
	}
}
