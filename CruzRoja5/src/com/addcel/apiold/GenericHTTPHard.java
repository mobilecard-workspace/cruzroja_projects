package com.addcel.apiold;

import java.io.UnsupportedEncodingException;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.util.security.AddcelCrypto;
import com.addcel.prosa.view.base.Viewable;




public class GenericHTTPHard extends HttpListenerExtend {

	public GenericHTTPHard(String post, String url, Viewable viewable) {
		super(post, url, viewable);
	}

	
	public GenericHTTPHard(Viewable viewable, String url, String post){
		super(viewable, url, post);
	}
	
	
	public void receiveHttpResponse(int appCode, byte[] response) {

		JSONObject jsObject = null;
		String json;
		try {
			
			json = new String(response, "UTF-8");
			json = AddcelCrypto.decryptHard(json);

			String newString = new String(json.getBytes("UTF-8"), "UTF-8");

			jsObject = new JSONObject(newString);
			sendData(jsObject);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			sendMessageError("Error en lectura de la respuesta");
		} catch (JSONException e) {
			e.printStackTrace();
			sendMessageError("Error en lectura de la respuesta");
		}
	}
}
